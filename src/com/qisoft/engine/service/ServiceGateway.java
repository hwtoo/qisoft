package com.qisoft.engine.service;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.qisoft.R;
import com.qisoft.engine.GlobalService;
import com.qisoft.xmlparse.onXmlParserCallBack;

public class ServiceGateway implements DialogInterface.OnClickListener {

	private onXmlParserCallBack xmlCallback = null;
	private Context context = null;
	private Bundle mParams;
	private final ProgressDialog mProgressDialog;

	public ServiceGateway(Context context) {
		this.context = context;
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage(context.getString(R.string.loadingMsg));
		mProgressDialog.setCanceledOnTouchOutside(true);
		mProgressDialog.setCancelable(true);
	}

	public void getData(onXmlParserCallBack xmlCallback, Bundle params,
			boolean loading) {
		if (loading && mProgressDialog != null && !mProgressDialog.isShowing())
			mProgressDialog.show();

		this.xmlCallback = xmlCallback;
		mParams = params;
		if (!GlobalService.isConnection(context)) {
			if (mProgressDialog != null && mProgressDialog.isShowing())
				mProgressDialog.dismiss();
			if (xmlCallback != null)
				this.xmlCallback.onConnectionError("CONNECTION_ERROR");
			return;
		}

		switch (params.getInt("CONTENT_ID")) {
		case 1000:// User Login
			userLogin(xmlCallback, params);
			return;
		case 1001:// User Register
			userRegister(xmlCallback, params);
			return;

		case 1002:// Get Club Statement Of Account Date
			getClubAccountDateList(xmlCallback, params);
			return;

		case 1003:// Forget Password
			resetPassword(xmlCallback, params);
			return;

		case 1004:// Get Available CLub
			getAllClub(xmlCallback, params);
			return;

		case 1005:// register Club
			registerClub(xmlCallback, params);
			return;

		case 1006:// logout User
			logoutUser(xmlCallback, params);
			return;

		case 1007:// Get Club Account Status
			getClubAccountStatus(xmlCallback, params);
			return;

		case 1008:// Get Update Profile
			getUpdateProfile(xmlCallback, params);
			return;

		case 1009:// Get CourseList
			getClubCourseList(xmlCallback, params);
			return;

		case 1010:// Get Tee Time
			getTeeTime(xmlCallback, params);
			return;

		case 1011:// Booking Tee Time
			BookTeeTime(xmlCallback, params);
			break;

		case 1012:// View Booking
			ViewBooking(xmlCallback, params);
			break;

		case 1013:// Delete Booking
			deleteBooking(xmlCallback, params);
			break;

		case 1014:// Booking History
			getBookingHistory(xmlCallback, params);
			break;

		case 1015:// update profile
			setUpdateProfile(xmlCallback, params);
			break;

		case 1016:// get SOA Date list
		case 1018:
			getSOADateList(xmlCallback, params);
			break;

		case 1017:// get Statement
		case 1019:
			getStatement(xmlCallback, params);
			break;

		}

	}

	private void userLogin(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param == null || param.get("email") == null
				|| param.get("password") == null
				|| param.get("deviceCD") == null)
			return;

		new getDataTask().execute(
				context.getString(R.string.apiURL) + "/mUserLogin?sLoginName="
						+ param.getString("email") + "&sPassword="
						+ param.getString("password") + "&sDeviceCD="
						+ param.getString("deviceCD") + "&sCheckSum="
						+ param.getString("sCheckSum"), param,
				param.get("CONTENT_ID"));
	}

	private void userRegister(onXmlParserCallBack xmlCallback, Bundle param) {
		// if (param == null || param.get("email") == null
		// || param.get("password") == null
		// || param.get("deviceCD") == null)
		// return;
		new getDataTask().execute(
				context.getString(R.string.apiURL) + "/mUserNew?sLoginName="
						+ param.getString("sLoginName") + "&sPassword="
						+ param.getString("sPassword") + "&sName="
						+ param.getString("sName") + "&dteDOB="
						+ param.getString("dteDOB") + "&sIcNo="
						+ param.getString("sIcNo") + "&sPassportno="
						+ param.getString("sPassportno") + "&sEmail="
						+ param.getString("sEmail") + "&sPhoneHP="
						+ param.getString("sPhoneHP") + "&sDeviceCD="
						+ param.getString("deviceCD") + "&sCheckSum="
						+ param.getString("sCheckSum"), param,
				param.get("CONTENT_ID"));
	}

	private void getClubAccountDateList(onXmlParserCallBack xmlCallback,
			Bundle param) {
		new getDataTask().execute(
				context.getString(R.string.apiURL)
						+ "/mClubSOADateList?sLoginCD="
						+ param.getString("sLoginCD") + "&sClubCD="
						+ param.getString("sClubCD") + "&sCheckSum="
						+ param.getString("sCheckSum") + "&sTokenCD="
						+ param.getString("sTokenCD") + "&sDeviceCD="
						+ param.getString("sDeviceCD"), param,
				param.get("CONTENT_ID"));
	}

	private void resetPassword(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("login_id") == null && param.get("email_id") == null)
			return;

		if (param.get("login_id") != null) {
			new getDataTask().execute(
					context.getString(R.string.apiURL)
							+ "/mUserForgetPasswordByLoginID?sLoginCD="
							+ param.getString("login_id") + "&sCheckSum="
							+ param.getString("sCheckSum"), param,
					param.get("CONTENT_ID"));
		} else if (param.get("email_id") != null) {
			new getDataTask().execute(
					context.getString(R.string.apiURL)
							+ "/mUserForgetPasswordByEmail?sEmailAddress="
							+ param.getString("email_id") + "&sCheckSum="
							+ param.getString("sCheckSum"), param,
					param.get("CONTENT_ID"));
		}
	}

	private void registerClub(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sMember") == null
				&& param.get("sClubCD") == null
				&& param.get("sCheckSum") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null)
			return;
		String url = context.getString(R.string.apiURL)
				+ "/mAuthorizationNew?sLoginCD=" + param.getString("sLoginCD")
				+ "&sMember=" + param.getString("sMember") + "&sClubCD="
				+ param.getString("sClubCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void logoutUser(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginName") == null && param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;
		String url = context.getString(R.string.apiURL)
				+ "/mUserLogout?sLoginName=" + param.getString("sLoginName")
				+ "&sTokenCD=" + param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getClubAccountStatus(onXmlParserCallBack xmlCallback,
			Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mClubAccountStatus?sLoginCD=" + param.getString("sLoginCD")
				+ "&sClubCD=" + param.get("sClubCD") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getUpdateProfile(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mClubUpdateProfileGet?sLoginCD="
				+ param.getString("sLoginCD") + "&sClubCD="
				+ param.get("sClubCD") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getClubCourseList(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sName") == null
				&& param.get("sClubCD") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mClubCourseList?sLoginCD=" + param.getString("sLoginCD")
				+ "&sName=" + param.getString("sName") + "&sClubCD="
				+ param.get("sClubCD") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getTeeTime(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("lClubCourseID") == null
				&& param.get("dteDate") == null
				&& param.get("dteSTime") == null
				&& param.get("dteETime") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mTeeTimeList?sLoginCD=" + param.getString("sLoginCD")
				+ "&lClubCourseID=" + param.getString("lClubCourseID")
				+ "&dteDate=" + param.get("dteDate") + "&dteSTime="
				+ param.getString("dteSTime") + "&dteETime="
				+ param.getString("dteETime") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void BookTeeTime(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("lClubCourseID") == null
				&& param.get("lTeeTimeID") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mTeeTimeBooking?sLoginCD=" + param.getString("sLoginCD")
				+ "&lClubCourseID=" + param.getString("lClubCourseID")
				+ "&lTeeTimeID=" + param.get("lTeeTimeID") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void ViewBooking(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mTeeTimeView?sLoginCD=" + param.getString("sLoginCD")
				+ "&sClubCD=" + param.get("sClubCD") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void deleteBooking(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("lBookingID") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mTeeTimeBookingDelete?sLoginCD="
				+ param.getString("sLoginCD") + "&sClubCD="
				+ param.get("sClubCD") + "&lBookingID="
				+ param.getString("lBookingID") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getBookingHistory(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null
				&& param.get("sCheckSum") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mTeeTimeViewHistory?sLoginCD="
				+ param.getString("sLoginCD") + "&sClubCD="
				+ param.get("sClubCD") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sDeviceCD="
				+ param.getString("sDeviceCD");
		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void setUpdateProfile(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sCheckSum") == null
				&& param.get("sFullName") == null
				&& param.get("sGender") == null && param.get("sDoB") == null
				&& param.get("sICNo") == null && param.get("sPassport") == null
				&& param.get("sHousePhone") == null
				&& param.get("sHandPhone") == null
				&& param.get("sEmail") == null
				&& param.get("sMailAddress") == null
				&& param.get("sMailPostCode") == null
				&& param.get("lSalutation") == null
				&& param.get("lNationality") == null
				&& param.get("lRace") == null && param.get("lReligion") == null
				&& param.get("lMailCity") == null
				&& param.get("lMailState") == null
				&& param.get("lMailCountry") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null)
			return;

		for (String key : param.keySet())
			System.out.println("%%%%%%% key =" + key + "  value="
					+ param.getString(key));

		String url = context.getString(R.string.apiURL)
				+ "/mClubUpdateProfileSet?sLoginCD="
				+ param.get("sLoginCD") + "&sClubCD="
				+ param.get("sClubCD") + "&sCheckSum="
				+ param.get("sCheckSum") + "&sFullName="
				+ param.get("sFullName") + "&sGender="
				+ param.get("sGender") + "&sDoB=" + param.get("sDoB")
				+ "&sICNo=" + param.get("sICNo") + "&sPassport="
				+ param.get("sPassport") + "&sHousePhone="
				+ param.get("sHousePhone") + "&sHandPhone="
				+ param.get("sHandPhone") + "&sEmail="
				+ param.get("sEmail") + "&sMailAddress="
				+ param.get("sMailAddress") + "&sMailPostCode="
				+ param.get("sMailPostCode") + "&lSalutation="
				+ param.get("lSalutation") + "&lNationality="
				+ param.get("lNationality") + "&lRace="
				+ param.get("lRace") + "&lReligion="
				+ param.get("lReligion") + "&lMailCity="
				+ param.get("lMailCity") + "&lMailState="
				+ param.get("lMailState") + "&lMailCountry="
				+ param.get("lMailCountry") + "&sTokenCD="
				+ param.get("sTokenCD") + "&sDeviceCD="
				+ param.get("sDeviceCD");

		new getDataTask().execute(url.replaceAll(" ", "%20").replaceAll("\n", "%0A"), param,
				param.get("CONTENT_ID"));
	}

	private void getSOADateList(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sCheckSum") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null)
			return;
		String url = "";
		if (param.getInt("CONTENT_ID") == 1016) {
			url = context.getString(R.string.apiURL)
					+ "/mClubSOADateList?sLoginCD="
					+ param.getString("sLoginCD") + "&sClubCD="
					+ param.getString("sClubCD") + "&sCheckSum="
					+ param.getString("sCheckSum") + "&sTokenCD="
					+ param.getString("sTokenCD") + "&sDeviceCD="
					+ param.getString("sDeviceCD");
		} else if (param.getInt("CONTENT_ID") == 1018) {
			System.out.println("%%%% content_id =" + 1018);
			url = context.getString(R.string.apiURL)
					+ "/mClubSOABonusPointDateList?sLoginCD="
					+ param.getString("sLoginCD") + "&sClubCD="
					+ param.getString("sClubCD") + "&sCheckSum="
					+ param.getString("sCheckSum") + "&sTokenCD="
					+ param.getString("sTokenCD") + "&sDeviceCD="
					+ param.getString("sDeviceCD");
		}

		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getStatement(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sClubCD") == null
				&& param.get("sCheckSum") == null
				&& param.get("dteSoaDate") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null)
			return;

		String url = "";
		if (param.getInt("CONTENT_ID") == 1017)
			url = context.getString(R.string.apiURL) + "/mClubSOA?sLoginCD="
					+ param.getString("sLoginCD") + "&sClubCD="
					+ param.getString("sClubCD") + "&sCheckSum="
					+ param.getString("sCheckSum") + "&dteSoaDate="
					+ param.getString("dteSoaDate") + "&sTokenCD="
					+ param.getString("sTokenCD") + "&sDeviceCD="
					+ param.getString("sDeviceCD");
		else if (param.getInt("CONTENT_ID") == 1019)
			url = context.getString(R.string.apiURL)
					+ "/mClubSOABonusPoint?sLoginCD="
					+ param.getString("sLoginCD") + "&sClubCD="
					+ param.getString("sClubCD") + "&sCheckSum="
					+ param.getString("sCheckSum") + "&dteSoaDate="
					+ param.getString("dteSoaDate") + "&sTokenCD="
					+ param.getString("sTokenCD") + "&sDeviceCD="
					+ param.getString("sDeviceCD");

		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private void getAllClub(onXmlParserCallBack xmlCallback, Bundle param) {
		if (param.get("sLoginCD") == null && param.get("sName") == null
				&& param.get("bFilterByLoginID") == null
				&& param.get("sCheckSum") == null
				&& param.get("sTokenCD") == null
				&& param.get("sDeviceCD") == null)
			return;

		String url = context.getString(R.string.apiURL)
				+ "/mClubList?sLoginCD=" + param.getString("sLoginCD")
				+ "&sName=" + param.getString("sName") + "&bFilterByLoginID="
				+ param.getBoolean("bFilterByLoginID") + "&sCheckSum="
				+ param.getString("sCheckSum") + "&sTokenCD="
				+ param.getString("sTokenCD") + "&sDeviceCD="
				+ param.getString("sDeviceCD");

		new getDataTask().execute(url.replaceAll(" ", "%20"), param,
				param.get("CONTENT_ID"));
	}

	private String executeGet(String url, Bundle extra) throws IOException {
		return executeRequest(new HttpGet(url), extra);
	}

	private String executeRequest(HttpUriRequest request, Bundle extra)
			throws IOException {
		DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
		request.setHeader("Content-type", "text/xml");
		// request.setHeader("Accept", "application/json");
		String str = "";
		for (Header head : request.getAllHeaders())
			if (head.getName().equals("Authorization"))
				System.out.println("VCVCVC name:" + head.getName() + " value:"
						+ head.getValue());
		try {
			str = (String) localDefaultHttpClient.execute(request,
					new HttpResponseHandler(extra));
			localDefaultHttpClient.getConnectionManager().shutdown();
			return str;

		} catch (ClientProtocolException e) {
			request.abort();
			throw new ClientProtocolException(e);
		} catch (IOException e) {
			throw new IOException(e);
		}
		// return str;
	}

	private class HttpResponseHandler implements ResponseHandler<String> {
		private Bundle extraParam = null;

		public HttpResponseHandler(Bundle extra) {
			super();
			extraParam = extra;
		}

		@Override
		public String handleResponse(HttpResponse httpResponse)
				throws ClientProtocolException, IOException {
			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				// System.out.println("zzz statusCode: "
				// + httpResponse.getStatusLine().getStatusCode());
				// throw new IOException("statusCode: "
				// + httpResponse.getStatusLine().getStatusCode());
			}
			HttpEntity localHttpEntity = httpResponse.getEntity();
			String result = "";
			long totalLength = localHttpEntity.getContentLength();
			InputStream localInputStream = localHttpEntity.getContent();

			byte[] arrayOfByte = new byte[1024];
			int length = 0;
			while ((length = localInputStream.read(arrayOfByte)) != -1) {
				result += new String(arrayOfByte, 0, length);
			}

			return result;
		}
	}

	private class getDataTask extends AsyncTask<Object, Integer, Object> {

		private Bundle localParam;
		private int contentId;

		@Override
		protected Object doInBackground(Object... params) {
			String printData = "";
			String url = "";

			if (params.length >= 1 && params[0] instanceof String)
				url = (String) params[0];

			if (params.length >= 2 && params[1] instanceof Bundle)
				localParam = (Bundle) params[1];

			if (params.length >= 3 && params[2] instanceof Integer)
				contentId = (Integer) params[2];

			Log.d("QiSoft URL", url);

			try {
				printData = executeGet(url, null);
				int length = printData.length();
				for (int i = 0; i < length; i += 1024) {
					if (i + 1024 < length)
						Log.d("QiSoft OUTPUT", printData.substring(i, i + 1024));
					else
						Log.d("QiSoft OUTPUT", printData.substring(i, length));
				}

				if (xmlCallback != null)
					return xmlCallback.xmlSerializerData(printData, localParam,
							contentId);
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				if (mProgressDialog != null && mProgressDialog.isShowing())
					mProgressDialog.dismiss();
				if (xmlCallback != null)
					xmlCallback.onConnectionError("CONNECTION_ERROR");
			}
			return printData;
		}

		@Override
		protected void onPostExecute(Object object) {

			if (xmlCallback != null)
				xmlCallback.onSerializerComplete(object, localParam, contentId);

			if (mProgressDialog != null && mProgressDialog.isShowing())
				mProgressDialog.dismiss();

			// Serializer serializer = new Persister();
			//
			// try {
			// if(xmlCallback!=null)
			// xmlCallback.xmlSerializerData(result)
			// UserEntity mEntity = (UserEntity) serializer.read(
			// UserEntity.class, result);
			//
			// System.out.println("%%%%%%****** mEntity.getIsStatus = "
			// + mEntity.getIsStatus());
			// System.out.println("%%%%%%****** mEntity.getLastLogin = "
			// + mEntity.getLastLogin());
			// System.out.println("%%%%%%****** mEntity.getMsg = "
			// + mEntity.getMsg());
			// System.out.println("%%%%%%****** mEntity.getName = "
			// + mEntity.getName());
			// System.out.println("%%%%%%****** mEntity.getToken = "
			// + mEntity.getToken());
			// System.out.println("%%%%%%****** mEntity.getmGsCd = "
			// + mEntity.getgsCD());
			//
			// GdatClub mClub = mEntity.getGdatClub();
			// List<UserClub> clubArry = mClub.getNewDataSet();
			// for (UserClub mUserClub : clubArry) {
			// System.out.println("%%%%%%****** mUserClub.getClubcd="
			// + mUserClub.getClubcd());
			// System.out.println("%%%%%%****** mUserClub.getId="
			// + mUserClub.getId());
			// System.out.println("%%%%%%****** mUserClub.getLogincd="
			// + mUserClub.getLogincd());
			// System.out.println("%%%%%%****** mUserClub.getName="
			// + mUserClub.getName());
			// System.out.println("%%%%%%****** mUserClub.getName="
			// + mUserClub.getSoption());
			// }
			//
			// GdatMenu mClubMenu = mEntity.getGdatMenu();
			// List<UserClubMenu> clubMenuArry = mClubMenu.getNewDataSet();
			// for (UserClubMenu mUserClubMenu : clubMenuArry) {
			// System.out.println("%%%%%%****** mUserClub.getClubcd="
			// + mUserClubMenu.getClubcd());
			// System.out.println("%%%%%%****** mUserClub.getHyperlink="
			// + mUserClubMenu.getHyperlink());
			// System.out.println("%%%%%%****** mUserClub.getLogincd="
			// + mUserClubMenu.getId());
			// System.out.println("%%%%%%****** mUserClub.getLogincd="
			// + mUserClubMenu.getLogincd());
			// System.out.println("%%%%%%****** mUserClub.getMenugroup="
			// + mUserClubMenu.getMenugroup());
			// System.out.println("%%%%%%****** mUserClub.getMenuname="
			// + mUserClubMenu.getMenuname());
			// System.out.println("%%%%%%****** mUserClub.getRowOrder="
			// + mUserClubMenu.getRowOrder());
			// System.out.println("%%%%%%****** mUserClub.getSeq="
			// + mUserClubMenu.getSeq());
			// System.out.println("%%%%%%****** mUserClub.getType="
			// + mUserClubMenu.getType());
			// System.out.println("%%%%%%****** mUserClub.getUserId="
			// + mUserClubMenu.getUserId());
			// }
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			super.onPostExecute(object);
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// if (which == DialogInterface.BUTTON_POSITIVE)
		// getData(xmlCallback, mParams);
	}
}
