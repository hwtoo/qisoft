package com.qisoft.xmlparse;

import android.os.Bundle;

public interface onXmlParserCallBack {

	public Object xmlSerializerData(String result, Bundle param, int contentID);

	public void onSerializerComplete(Object object, Bundle param, int contentID);

	public void onConnectionError(String errorMsg);
}
