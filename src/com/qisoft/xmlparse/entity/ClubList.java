package com.qisoft.xmlparse.entity;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "ReturnResult", strict = false)
public class ClubList {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;
	
	@Element(required = false)
	DatParam1 datParam1;
	
	public DatParam1 getdatParam1() {
		return datParam1;
	}

//	@ElementList
//	@Namespace(prefix = "")
//	protected List<UserClubLIst> NewDataSet;
//
//	public List<UserClubLIst> getNewDataSet() {
//		return NewDataSet;
//	}
//
//	public void setNewDataSet(List<UserClubLIst> newDataSet) {
//		NewDataSet = newDataSet;
//	}

	public String getsMsg() {
		return sMsg;
	}

	public void setsMsg(String sMsg) {
		this.sMsg = sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}

	public void setiStatus(int iStatus) {
		this.iStatus = iStatus;
	}

}
