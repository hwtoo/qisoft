package com.qisoft.xmlparse.entity;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name="gdatMenu",strict=false)
public class GdatMenu {

	@Element
	protected String diffgram;
	
	@ElementList
	@Namespace(prefix = "")
	protected List<UserClubMenu> NewDataSet;

	public List<UserClubMenu> getNewDataSet() {
		return NewDataSet;
	}
	
}
