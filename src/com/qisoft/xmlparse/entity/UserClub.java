package com.qisoft.xmlparse.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Element(name = "UserClub")
public class UserClub {

	@Attribute
	@Namespace(prefix = "diffgr", reference = "urn:schemas-microsoft-com:xml-diffgram-v1")
	String id;

	@Attribute
	@Namespace(prefix = "msdata", reference = "urn:schemas-microsoft-com:xml-msdata")
	String rowOrder;

	@Element
	String logincd;

	@Element
	String clubcd;

	@Element
	String name;

	@Element
	String soption;

	String optionAry[];

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogincd() {
		return logincd;
	}

	public void setLogincd(String logincd) {
		this.logincd = logincd;
	}

	public String getClubcd() {
		return clubcd;
	}

	public void setClubcd(String clubcd) {
		this.clubcd = clubcd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSoption() {
		return soption;
	}

	public boolean getOptionFlag(int optionNumber) {
		char flag = soption.charAt(optionNumber - 1);
		return flag == '1' ? true : false;
	}

	public boolean getOptionFlag(int... optionNumbe) {
		for (int option : optionNumbe) {
			char flag = soption.charAt(option - 1);
			System.out.println("%%%%% flag ="+flag);
			if (flag != '1')
				return false;
		}
		return true;
	}

	public void setSoption(String soption) {
		this.soption = soption;
	}

}
