package com.qisoft.xmlparse.entity.profileGet;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class DataClass {

	@Element()
	int id;
	@Element()
	String descs;
	
	String gender;
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getGender() {
		return gender;
	}

	public int getId() {
		return id;
	}

	public String getDescs() {
		return descs;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDescs(String descs) {
		this.descs = descs;
	}
	
	
}
