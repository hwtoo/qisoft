package com.qisoft.xmlparse.entity.profileGet;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ProfileDatParam1 {

	@Element
	protected String diffgram;

	@ElementList
	protected List<ClubUpdateProfileGet> NewDataSet;

	public ClubUpdateProfileGet getUserProfile() {
		return NewDataSet.get(0);
	}
}
