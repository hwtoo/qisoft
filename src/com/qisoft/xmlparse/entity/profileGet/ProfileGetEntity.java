package com.qisoft.xmlparse.entity.profileGet;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ReturnResult", strict = false)
public class ProfileGetEntity {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;

	@Element(required = false)
	ProfileDatParam1 datParam1;

	@Element(required = false)
	ProfileDatParam2 datParam2;

	@Element(required = false)
	ProfileDatParam3 datParam3;

	@Element(required = false)
	ProfileDatParam4 datParam4;

	@Element(required = false)
	ProfileDatParam5 datParam5;

	@Element(required = false)
	ProfileDatParam6 datParam6;

	@Element(required = false)
	ProfileDatParam7 datParam7;

	@Element(required = false)
	ProfileDatParam8 datParam8;

	public ProfileDatParam1 getDatParam1() {
		return datParam1;
	}

	public ProfileDatParam2 getDatParam2() {
		return datParam2;
	}

	public ProfileDatParam3 getDatParam3() {
		return datParam3;
	}

	public ProfileDatParam4 getDatParam4() {
		return datParam4;
	}

	public ProfileDatParam5 getDatParam5() {
		return datParam5;
	}

	public ProfileDatParam6 getDatParam6() {
		return datParam6;
	}

	public ProfileDatParam7 getDatParam7() {
		return datParam7;
	}

	public ProfileDatParam8 getDatParam8() {
		return datParam8;
	}

	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
}
