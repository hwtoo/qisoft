package com.qisoft.xmlparse.entity.profileGet;

import java.util.Date;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ClubUpdateProfileGet {
	@Element
	String cd;
	@Element(required = false)
	int salutationid;
	@Element(required = false)
	String fullname;
	@Element(required = false)
	Date dob;
	@Element(required = false)
	String gender;
	@Element(required = false)
	String icno;
	@Element(required = false)
	String passportno;
	@Element(required = false)
	int nationalityid;
	@Element(required = false)
	int religionid;
	@Element(required = false)
	int raceid;
	@Element(required = false)
	String housephone;
	@Element(required = false)
	String handphone;
	@Element(required = false)
	String emailaddress;
	@Element(required = false)
	String mailaddress;
	@Element(required = false)
	String mailpostcode;
	@Element(required = false)
	int mailcityid;
	@Element(required = false)
	int mailstateid;
	@Element(required = false)
	int mailcountryid;

	public String getCd() {
		return cd;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public int getSalutationid() {
		return salutationid;
	}

	public void setSalutationid(int salutationid) {
		this.salutationid = salutationid;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIcno() {
		return icno;
	}

	public void setIcno(String icno) {
		this.icno = icno;
	}

	public String getPassportno() {
		return passportno;
	}

	public void setPassportno(String passportno) {
		this.passportno = passportno;
	}

	public int getNationalityid() {
		return nationalityid;
	}

	public void setNationalityid(int nationalityid) {
		this.nationalityid = nationalityid;
	}

	public int getReligionid() {
		return religionid;
	}

	public void setReligionid(int religionid) {
		this.religionid = religionid;
	}

	public int getRaceid() {
		return raceid;
	}

	public void setRaceid(int raceid) {
		this.raceid = raceid;
	}

	public String getHousephone() {
		return housephone;
	}

	public void setHousephone(String housephone) {
		this.housephone = housephone;
	}

	public String getHandphone() {
		return handphone;
	}

	public void setHandphone(String handphone) {
		this.handphone = handphone;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	public String getMailaddress() {
		return mailaddress;
	}

	public void setMailaddress(String mailaddress) {
		this.mailaddress = mailaddress;
	}

	public String getMailpostcode() {
		return mailpostcode;
	}

	public void setMailpostcode(String mailpostcode) {
		this.mailpostcode = mailpostcode;
	}

	public int getMailcityid() {
		return mailcityid;
	}

	public void setMailcityid(int mailcityid) {
		this.mailcityid = mailcityid;
	}

	public int getMailstateid() {
		return mailstateid;
	}

	public void setMailstateid(int mailstateid) {
		this.mailstateid = mailstateid;
	}

	public int getMailcountryid() {
		return mailcountryid;
	}

	public void setMailcountryid(int mailcountryid) {
		this.mailcountryid = mailcountryid;
	}

}
