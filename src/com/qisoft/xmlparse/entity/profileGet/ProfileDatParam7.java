package com.qisoft.xmlparse.entity.profileGet;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ProfileDatParam7 {

	@Element
	protected String diffgram;

	@ElementList(entry="State")
	@Namespace(prefix = "")
	protected List<DataClass> NewDataSet;

	public List<DataClass> getNewDataSet() {
		return NewDataSet;
	}

	public void setNewDataSet(List<DataClass> newDataSet) {
		NewDataSet = newDataSet;
	}
}
