package com.qisoft.xmlparse.entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ReturnResult", strict = false)
public class StatusEntity {
	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;

	public String getsMsg() {
		return sMsg;
	}

	public void setsMsg(String sMsg) {
		this.sMsg = sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}

	public void setiStatus(int iStatus) {
		this.iStatus = iStatus;
	}

}
