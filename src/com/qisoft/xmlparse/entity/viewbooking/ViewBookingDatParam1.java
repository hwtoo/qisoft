package com.qisoft.xmlparse.entity.viewbooking;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ViewBookingDatParam1 {

	@Element(required = false)
	protected String diffgram;

	@ElementList(required = false)
	protected List<TeeTimeViewEntity> NewDataSet;

	public List<TeeTimeViewEntity> getTeeTimeList() {
		return NewDataSet;
	}
}
