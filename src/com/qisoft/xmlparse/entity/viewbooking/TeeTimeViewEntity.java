package com.qisoft.xmlparse.entity.viewbooking;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class TeeTimeViewEntity {

	@Element(required = false)
	int bookingid;

	@Element(required = false)
	String ref;

	@Element(required = false)
	String displaydate;

	@Element(required = false)
	String displaytime;

	@Element(required = false)
	String usertrx;

	public int getBookingid() {
		return bookingid;
	}

	public String getRef() {
		return ref;
	}

	public String getDisplaydate() {
		return displaydate;
	}

	public String getDisplaytime() {
		return displaytime;
	}

	public String getUsertrx() {
		return usertrx;
	}

}
