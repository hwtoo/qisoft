package com.qisoft.xmlparse.entity.teetime;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "ReturnResult")
public class TeeTimeEntity {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;
	@Element(required = false)
	String sParam1 = "";
	@Element(required = false)
	String sParam2 = "";
	@Element(required = false)
	String sParam3 = "";
	@Element(required = false)
	String sParam4 = "";
	@Element(required = false)
	String sParam5 = "";
	@Element(required = false)
	String sParam6 = "";
	@Element(required = false)
	String sParam7 = "";

	@Element(required = false)
	TeeTimeDatParam1 datParam1;

	@Element(required = false)
	TeeTimeDatParam2 datParam2;
	@Element(required = false)
	TeeTimeDatParam3 datParam3;

	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}

	public String getsParam1() {
		return sParam1;
	}

	public String getsParam2() {
		return sParam2;
	}

	public String getsParam3() {
		return sParam3;
	}

	public String getsParam4() {
		return sParam4;
	}

	public String getsParam5() {
		return sParam5;
	}

	public String getsParam6() {
		return sParam6;
	}

	public String getsParam7() {
		return sParam7;
	}

	public TeeTimeDatParam1 getDatParam1() {
		return datParam1;
	}

	public TeeTimeDatParam2 getDatParam2() {
		return datParam2;
	}

	public TeeTimeDatParam3 getDatParam3() {
		return datParam3;
	}

}
