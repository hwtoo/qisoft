package com.qisoft.xmlparse.entity.teetime;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class TeeTimeList {

	@Element(required = false)
	String teetime;
	@Element(required = false)
	int teetimeid;

	public String getTeeTime() {
		return teetime;
	}

	public int getTeeTimeId() {
		return teetimeid;
	}

}
