package com.qisoft.xmlparse.entity.teetime;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class TeeTimeDatParam3 {

	@Element
	protected String diffgram;

	@ElementList(entry = "TeeTimeList3")
	protected List<TeeTimeList> NewDataSet;

	public List<TeeTimeList> getTeeTimeList() {
		return NewDataSet;
	}
}
