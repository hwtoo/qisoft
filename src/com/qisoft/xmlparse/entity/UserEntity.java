package com.qisoft.xmlparse.entity;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "clsLogin", strict = false)
public class UserEntity {
	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;
	@Element(required = false)
	String gsName = "";
	@Element(required = false)
	String gdteLastLogin = "";
	@Element(required = false)
	String sToken = "";
	@Element(required = false)
	String gsCD = "";

	@Element(required = false)
	GdatClub gdatClub;

	@Element(required = false)
	GdatMenu gdatMenu;


	public GdatMenu getGdatMenu() {
		return gdatMenu;
	}

	public void setGdatMenu(GdatMenu gdatMenu) {
		this.gdatMenu = gdatMenu;
	}

	public GdatClub getGdatClub() {
		return gdatClub;
	}

	public void setGdatClub(GdatClub gdatClub) {
		this.gdatClub = gdatClub;
	}

	public String getgsCD() {
		return gsCD;
	}

	public void setgsCD(String gsCD) {
		this.gsCD = gsCD;
	}

	public String getMsg() {
		return sMsg;
	}

	public void setMsg(String msg) {
		this.sMsg = msg;
	}

	public int getIsStatus() {
		return iStatus;
	}

	public void setIsStatus(int isStatus) {
		this.iStatus = isStatus;
	}

	public String getName() {
		return gsName;
	}

	public void setName(String gsName) {
		this.gsName = gsName;
	}

	public String getLastLogin() {
		return gdteLastLogin;
	}

	public void setLastLogin(String gdteLastLogin) {
		this.gdteLastLogin = gdteLastLogin;
	}

	public String getToken() {
		return sToken;
	}

	public void setToken(String token) {
		this.sToken = token;
	}

	public List<UserClub> getUserClubList() {
		return gdatClub.getNewDataSet();
	}
	public UserClub getUserClubByNamess(String clubName) {
		for (UserClub mClub : gdatClub.getNewDataSet())
			if (mClub.getName().equals(clubName))
				return mClub;
		return null;
	}

}
