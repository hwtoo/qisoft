package com.qisoft.xmlparse.entity.accountStatus;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "datParam1", strict = false)
public class AccountDatParam1 {

	@Element
	protected String diffgram;

	@ElementList
	@Namespace(prefix = "")
	protected List<ClubAccountStatus> NewDataSet;

	public List<ClubAccountStatus> getNewDataSet() {
		return NewDataSet;
	}

	public void setNewDataSet(List<ClubAccountStatus> newDataSet) {
		NewDataSet = newDataSet;
	}
}
