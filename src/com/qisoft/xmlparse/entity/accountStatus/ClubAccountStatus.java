package com.qisoft.xmlparse.entity.accountStatus;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ClubAccountStatus", strict = false)
public class ClubAccountStatus {
	@Element()
	String cd;
	@Element()
	String fullname;
	@Element()
	String membertype;
	@Element()
	String statusdescs;
	@Element()
	double signlimit;
	@Element()
	double aging0;
	@Element()
	double aging1;
	@Element()
	double aging2;
	@Element()
	double aging3;
	@Element()
	double aging4;
	@Element()
	double agingsum5;
	@Element()
	double signamt;

	public String getCd() {
		return cd;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMembertype() {
		return membertype;
	}

	public void setMembertype(String membertype) {
		this.membertype = membertype;
	}

	public String getStatusdescs() {
		return statusdescs;
	}

	public void setStatusdescs(String statusdescs) {
		this.statusdescs = statusdescs;
	}

	public double getSignlimit() {
		return signlimit;
	}

	public void setSignlimit(double signlimit) {
		this.signlimit = signlimit;
	}

	public double getAging0() {
		return aging0;
	}

	public void setAging0(double aging0) {
		this.aging0 = aging0;
	}

	public double getAging1() {
		return aging1;
	}

	public void setAging1(double aging1) {
		this.aging1 = aging1;
	}

	public double getAging2() {
		return aging2;
	}

	public void setAging2(double aging2) {
		this.aging2 = aging2;
	}

	public double getAging3() {
		return aging3;
	}

	public void setAging3(double aging3) {
		this.aging3 = aging3;
	}

	public double getAging4() {
		return aging4;
	}

	public void setAging4(double aging4) {
		this.aging4 = aging4;
	}

	public double getAgingsum5() {
		return agingsum5;
	}

	public void setAgingsum5(double agingsum5) {
		this.agingsum5 = agingsum5;
	}

	public double getSignamt() {
		return signamt;
	}

	public void setSignamt(double signamt) {
		this.signamt = signamt;
	}

}
