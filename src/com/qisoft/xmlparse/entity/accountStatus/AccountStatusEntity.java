package com.qisoft.xmlparse.entity.accountStatus;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ReturnResult", strict = false)
public class AccountStatusEntity {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;

	@Element(required = false)
	AccountDatParam1 datParam1;

	public AccountDatParam1 getdatParam1() {
		return datParam1;
	}

	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
}
