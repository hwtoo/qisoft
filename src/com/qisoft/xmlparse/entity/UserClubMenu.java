package com.qisoft.xmlparse.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;

@Element(name = "UserClubMenu")
public class UserClubMenu {
	@Attribute
	@Namespace(prefix = "diffgr", reference = "urn:schemas-microsoft-com:xml-diffgram-v1")
	String id;

	@Attribute
	@Namespace(prefix = "msdata", reference = "urn:schemas-microsoft-com:xml-msdata")
	String rowOrder;

	@Element
	String logincd;
	@Element(name = "id")
	int UserId;
	@Element
	String clubcd;
	@Element
	String menugroup;
	@Element
	String menuname;
	@Element
	int type;
	@Element
	int seq;
	@Element
	String hyperlink;
	@Element
	boolean iscancel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRowOrder() {
		return rowOrder;
	}

	public void setRowOrder(String rowOrder) {
		this.rowOrder = rowOrder;
	}

	public String getLogincd() {
		return logincd;
	}

	public void setLogincd(String logincd) {
		this.logincd = logincd;
	}

	public int getUserId() {
		return UserId;
	}

	public void setUserId(int userId) {
		UserId = userId;
	}

	public String getClubcd() {
		return clubcd;
	}

	public void setClubcd(String clubcd) {
		this.clubcd = clubcd;
	}

	public String getMenugroup() {
		return menugroup;
	}

	public void setMenugroup(String menugroup) {
		this.menugroup = menugroup;
	}

	public String getMenuname() {
		return menuname;
	}

	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getHyperlink() {
		return hyperlink;
	}

	public void setHyperlink(String hyperlink) {
		this.hyperlink = hyperlink;
	}

	public boolean isIscancel() {
		return iscancel;
	}

	public void setIscancel(boolean iscancel) {
		this.iscancel = iscancel;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	// <logincd>khphua</logincd>
	// <id>2</id>
	// <clubcd>1QISOFT</clubcd>
	// <menugroup>Anual Report</menugroup>
	// <menuname>Year 2014</menuname>
	// <type>1</type>
	// <seq>1</seq>
	// <hyperlink>http://www.qisoft.com.my</hyperlink>
	// <iscancel>0</iscancel>
}
