package com.qisoft.xmlparse.entity.soadatelist;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class SOADateDatParam1 {

	@Element
	protected String diffgram;

	@ElementList(required = false)
	protected List<ClubSOADateList> NewDataSet;

	public List<ClubSOADateList> getClubSOADateList() {
		return NewDataSet;
	}
}
