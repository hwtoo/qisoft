package com.qisoft.xmlparse.entity.soadatelist;

import java.util.Date;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ClubSOADateList {

	@Element(required = false)
	String DateList;

	@Element(required = false)
	Date trxdate;
	

	public String getDateList() {
		return DateList;
	}

	public void setDateList(String dateList) {
		DateList = dateList;
	}

	public Date getTrxdate() {
		return trxdate;
	}

//	public void setTrxdate(String trxdate) {
//		this.trxdate = trxdate;
//	}

}
