package com.qisoft.xmlparse.entity.soadatelist;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "ReturnResult")
public class SOADateLIstEntity {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;
	

	@Element(required = false)
	SOADateDatParam1 datParam1;
	
	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
	
	public SOADateDatParam1 getDatParam1() {
		return datParam1;
	}

}
