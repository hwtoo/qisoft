package com.qisoft.xmlparse.entity;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "datParam1", strict = false)
public class DatParam1 {

	@Element
	protected String diffgram;

	@ElementList
	@Namespace(prefix = "")
	protected List<UserClubLIst> NewDataSet;

	public List<UserClubLIst> getNewDataSet() {
		return NewDataSet;
	}

	public void setNewDataSet(List<UserClubLIst> newDataSet) {
		NewDataSet = newDataSet;
	}
}
