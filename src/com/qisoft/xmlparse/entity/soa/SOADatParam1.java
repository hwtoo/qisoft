package com.qisoft.xmlparse.entity.soa;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class SOADatParam1 {

	@Element
	protected String diffgram;

	@ElementList()
	protected List<ClubSOA> NewDataSet;

	public List<ClubSOA> getClubSOADateList() {
		return NewDataSet;
	}
}
