package com.qisoft.xmlparse.entity.soa;

import java.util.Date;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ClubSOA {

	@Element(required = false)
	String departdescs;

	@Element(required = false)
	Date trxdate;

	@Element(required = false)
	double amount;

	@Element(required = false)
	double bpoint;

	@Element(required = false)
	String ref;

	double total = -1;

	public void setTotal(double total) {
		this.total = total;
	}

	public double getTotal() {
		return total;
	}

	public String getDepartdescs() {
		return departdescs;
	}

	public void setDepartdescs(String departdescs) {
		this.departdescs = departdescs;
	}

	public Date getTrxdate() {
		return trxdate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public double getBpoint() {
		return bpoint;
	}

}
