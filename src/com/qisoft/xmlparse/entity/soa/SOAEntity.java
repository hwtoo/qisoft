package com.qisoft.xmlparse.entity.soa;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "ReturnResult")
public class SOAEntity {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;
	

	@Element(required = false)
	SOADatParam1 datParam1;
	
	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
	
	public SOADatParam1 getDatParam1() {
		return datParam1;
	}

}
