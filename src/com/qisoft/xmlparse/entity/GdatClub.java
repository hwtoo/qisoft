package com.qisoft.xmlparse.entity;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "gdatClub",strict=false)
public class GdatClub {

	// @Element(required = false)
	// String schema;

	// @ElementList(inline = true, entry = "NewDataSet")
	
	@Element
	protected String diffgram;
	
	@ElementList
	@Namespace(prefix = "")
	protected List<UserClub> NewDataSet;

	public List<UserClub> getNewDataSet() {
		return NewDataSet;
	}

	public void setNewDataSet(List<UserClub> NewDataSet) {
		this.NewDataSet = NewDataSet;
	}

	// public NewDataSetList getNewDataList() {
	// return newDataList;
	// }
	//
	// public void setNewDataList(NewDataSetList newDataList) {
	// this.newDataList = newDataList;
	// }
}
