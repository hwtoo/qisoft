package com.qisoft.xmlparse.entity.teetimebooking;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class TeeTimeBookingDatParam1 {

	@Element
	protected String diffgram;

	@ElementList
	protected List<TeeTimeBooking> NewDataSet;

	public List<TeeTimeBooking> getTeeTimeList() {
		return NewDataSet;
	}

	public String getRefNumber() {
		return NewDataSet.get(0).getRef();
	}
}
