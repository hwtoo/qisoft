package com.qisoft.xmlparse.entity.teetimebooking;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class TeeTimeBooking {

	@Element(required = false)
	String ref;

	public String getRef() {
		return ref;
	}

}
