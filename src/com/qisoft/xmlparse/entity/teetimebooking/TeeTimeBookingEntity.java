package com.qisoft.xmlparse.entity.teetimebooking;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "ReturnResult")
public class TeeTimeBookingEntity {
	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;

	@Element(required = false)
	TeeTimeBookingDatParam1 datParam1;

	public TeeTimeBookingDatParam1 getDatParam1() {
		return datParam1;
	}

	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
}
