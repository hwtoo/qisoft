package com.qisoft.xmlparse.entity.history;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "ReturnResult")
public class HistoryBookingEntity {
	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;

	@Element(required = false)
	HistoryBookingDatParam1 datParam1;

	public HistoryBookingDatParam1 getDatParam1() {
		return datParam1;
	}

	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
}
