package com.qisoft.xmlparse.entity.history;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.qisoft.xmlparse.entity.viewbooking.TeeTimeViewEntity;

@Root(strict = false)
public class HistoryBookingDatParam1 {

	@Element
	protected String diffgram;

	@ElementList(entry = "TeeTimeViewHistory")
	protected List<TeeTimeViewEntity> NewDataSet;

	public List<TeeTimeViewEntity> getTeeTimeList() {
		return NewDataSet;
	}
}
