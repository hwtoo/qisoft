package com.qisoft.xmlparse.entity.clubCourseList;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class CourseDatParam2 {

	@Element
	protected String diffgram;

	@ElementList
	protected List<ClubCourseDateList> NewDataSet;

	public List<ClubCourseDateList> getNewDataSet() {
		return NewDataSet;
	}
}
