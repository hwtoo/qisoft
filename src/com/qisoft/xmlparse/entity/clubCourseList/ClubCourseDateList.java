package com.qisoft.xmlparse.entity.clubCourseList;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ClubCourseDateList {

	@Element
	String DateName;

	@Element
	String DateValue;

	public String getDateName() {
		return DateName;
	}

	public String getDateValue() {
		return DateValue;
	}

}
