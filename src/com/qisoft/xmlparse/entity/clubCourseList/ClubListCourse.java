package com.qisoft.xmlparse.entity.clubCourseList;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
class ClubListCourse {

	@Element(required = false)
	int clubcourseid;

	@Element(required = false)
	String Name;

	@Element(required = false)
	int sessiontype;

	public int getClubcourseid() {
		return clubcourseid;
	}

	public int getSessiontype() {
		return sessiontype;
	}

	public String getName() {
		return Name;
	}

}
