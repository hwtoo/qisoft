package com.qisoft.xmlparse.entity.clubCourseList;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ReturnResult", strict = false)
public class ClubCourseList {

	@Element(required = false)
	String sMsg = "";
	@Element
	int iStatus = -1;

	@Element(required = false)
	CourseDatParam1 datParam1;

	@Element(required = false)
	CourseDatParam2 datParam2;

	public CourseDatParam1 getDatParam1() {
		return datParam1;
	}

	public CourseDatParam2 getDatParam2() {
		return datParam2;
	}

	public String getsMsg() {
		return sMsg;
	}

	public int getiStatus() {
		return iStatus;
	}
}
