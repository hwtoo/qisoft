package com.qisoft.xmlparse.entity.clubCourseList;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class CourseDatParam1 {

	@Element
	protected String diffgram;

	@ElementList
	protected List<ClubListCourse> NewDataSet;

	public int getClubCourseID() {
		return NewDataSet.get(0).getClubcourseid();
	}

	public String getClubCourseName() {
		return NewDataSet.get(0).getName();
	}
}
