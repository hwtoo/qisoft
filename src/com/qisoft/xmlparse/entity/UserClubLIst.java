package com.qisoft.xmlparse.entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ClubList", strict = false)
public class UserClubLIst {
	@Element()
	String cd = "";
	@Element()
	String Name = "";

	public String getCd() {
		return cd;
	}

	public String getName() {
		return Name;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public void setName(String name) {
		this.Name = name;
	}
}
