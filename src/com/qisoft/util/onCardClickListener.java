package com.qisoft.util;

import com.qisoft.xmlparse.entity.viewbooking.TeeTimeViewEntity;

public interface onCardClickListener {

	public void onCardClick(TeeTimeViewEntity mTeeTimeEntity);
}
