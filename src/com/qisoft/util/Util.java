package com.qisoft.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.qisoft.view.dialog.CustomTimePickerDialog;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.widget.NumberPicker;
import android.widget.TimePicker;

public class Util {

	private static SimpleDateFormat dateFormatter = new SimpleDateFormat(
			"dd-MMM-yyyy");

	public static String round(double paramDouble) {
		return new DecimalFormat("#,###,##0.00").format(paramDouble);
	}

	public static String getDOBInText(Calendar calendar) {
		return dateFormatter.format(calendar.getTime());
	}

	public static void getDateFromDatePicker(int Viewid, Context context,
			OnDateSetListener dateSeletListner, Calendar minDate,
			Calendar maxDate) {
		DatePickerDialog picker = new DatePickerDialog(context,
				dateSeletListner, minDate.get(Calendar.YEAR),
				minDate.get(Calendar.MONTH), minDate.get(Calendar.DAY_OF_MONTH));
		if (minDate != null)
			picker.getDatePicker().setMinDate(minDate.getTimeInMillis());
		if (maxDate != null)
			picker.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
		picker.getDatePicker().setTag(Viewid);
		picker.show();
	}

	public static void getTimeFromTimePicker(int Viewid, Context context,
			OnTimeSetListener timeSetListner, int startHour) {
		new CustomTimePickerDialog(Viewid, context, timeSetListner, startHour,
				0, false).show();
	}

}
