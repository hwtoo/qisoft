package com.qisoft.adapter;

import java.util.ArrayList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.util.onCardClickListener;
import com.qisoft.xmlparse.entity.viewbooking.TeeTimeViewEntity;

public class ViewBookingRecyclerViewAdapter extends
		RecyclerView.Adapter<ViewBookingRecyclerViewAdapter.ViewHolder> {

	private ArrayList<TeeTimeViewEntity> data ;
	private onCardClickListener listener = null;

	public ViewBookingRecyclerViewAdapter(onCardClickListener listener) {
		this.listener = listener;
		data = new ArrayList<TeeTimeViewEntity>();
	}

	public void addData(TeeTimeViewEntity en) {
		data.add(en);
	}

	public boolean isEmpty() {
		return data.isEmpty();
	}

	public void clean() {
		data.clear();
	}

	@Override
	public int getItemCount() {
		return data.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		System.out.println("%%%% onBindView Holder");
		final TeeTimeViewEntity en = data.get(position);
		holder.bookingDateTextView.setText(": " + en.getDisplaydate());
		holder.bookingTeeTimeTextView.setText(": " + en.getDisplaytime());
		holder.bookedByTextView.setText(": " + en.getUsertrx());
		holder.bookingNumberTextView.setText(": " + en.getBookingid());
		holder.deleteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listener != null)
					listener.onCardClick(en);
			}
		});
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		System.out.println("%%%% onCreateViewHolder");
		View v = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.child_booking_card, parent, false);
		ViewHolder vh = new ViewHolder(v);
		return vh;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		public TextView bookingDateTextView = null,
				bookingTeeTimeTextView = null, bookingNumberTextView = null,
				bookedByTextView = null;
		public Button deleteBtn = null;

		public ViewHolder(View v) {
			super(v);
			bookingDateTextView = (TextView) v
					.findViewById(R.id.bookingDateTextView);
			bookingTeeTimeTextView = (TextView) v
					.findViewById(R.id.bookingTeeTimeTextView);
			bookingNumberTextView = (TextView) v
					.findViewById(R.id.bookingNumberTextView);
			bookedByTextView = (TextView) v.findViewById(R.id.bookedByTextView);
			deleteBtn = (Button) v.findViewById(R.id.deleteBtn);
		}

	}
}
