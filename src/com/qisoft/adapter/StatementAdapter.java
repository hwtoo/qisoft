package com.qisoft.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.util.Util;
import com.qisoft.xmlparse.entity.soa.ClubSOA;

public class StatementAdapter extends BaseAdapter {

	private List<ClubSOA> data = new ArrayList<ClubSOA>();
	private LayoutInflater inflater = null;
	private double total = -1;
	private boolean isAcountStatment = false;

	public StatementAdapter(Context context, boolean isAcountStatment) {
		inflater = LayoutInflater.from(context);
		this.isAcountStatment = isAcountStatment;
	}

	public void addData(List<ClubSOA> Array) {
		if (!data.isEmpty())
			data.clear();
		data = Array;
	}

	public void cleanUp() {
		data.clear();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	// public String getTrxdate(int position) {
	// return data.get(position).getTrxdate();
	// }

	// public int getPositionById(int id) {
	// for (int i = 0; i < data.size(); i++) {
	// DataClass dataClass = data.get(i);
	// if (dataClass.getId() == id)
	// return i;
	// }
	// return 0;
	// }

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.child_statement, parent,
					false);
			holder.dateID = (TextView) convertView.findViewById(R.id.dateID);
			holder.referenceID = (TextView) convertView
					.findViewById(R.id.referenceID);
			holder.DescID = (TextView) convertView.findViewById(R.id.DescID);
			holder.AmountID = (TextView) convertView
					.findViewById(R.id.AmountID);
			holder.totalID = (TextView) convertView.findViewById(R.id.totalID);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		ClubSOA entity = data.get(position);

		if (position == 0) {
			if (isAcountStatment)
				total = entity.getAmount();
			else
				total = entity.getBpoint();
		} else {
			if (isAcountStatment)
				total = total + entity.getAmount();
			else
				total = total + entity.getBpoint();
		}

		if (entity.getTotal() == -1) {
			entity.setTotal(total);
		}

		if (holder.dateID != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(entity.getTrxdate());
			holder.dateID.setText(Util.getDOBInText(calendar));
		}

		if (holder.referenceID != null)
			if (position == 0)
				holder.referenceID.setText("");
			else
				holder.referenceID.setText(entity.getRef());

		if (holder.DescID != null)
			holder.DescID.setText(entity.getDepartdescs());

		if (holder.AmountID != null) {
			if (isAcountStatment) {
				if (entity.getAmount() < 0) {
					holder.AmountID.setTextColor(Color.BLUE);
				} else
					holder.AmountID.setTextColor(Color.RED);

				if (position == 0)
					holder.AmountID.setText("");
				else
					holder.AmountID.setText(Util.round(entity.getAmount()));
			} else {
				if (entity.getBpoint() >= 0) {
					holder.AmountID.setTextColor(Color.BLUE);
				} else
					holder.AmountID.setTextColor(Color.RED);
				holder.AmountID.setText(Util.round(entity.getBpoint()));
			}

		}

		if (holder.totalID != null) {
			holder.totalID.setText(Util.round(entity.getTotal()));
		}

		return convertView;
	}

	public double getTotal() {
		return total;
	}

	class ViewHolder {
		TextView dateID, referenceID, DescID, AmountID, totalID;
	}

}
