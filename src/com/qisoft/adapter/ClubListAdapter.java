package com.qisoft.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.UserClub;

public class ClubListAdapter extends BaseAdapter {

	private ArrayList<UserClub> data = null;
	private LayoutInflater inflater = null;

	private int childResourceID = -1;

	public ClubListAdapter(Context context, int childResourceID) {
		if (data == null)
			data = new ArrayList<UserClub>();
		if (!data.isEmpty())
			data.clear();
		inflater = LayoutInflater.from(context);
		this.childResourceID = childResourceID;
	}

	public void addData(UserClub userClub) {
		data.add(userClub);
	}

	public void cleanUp() {
		data.clear();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public String setClubcdByName(String clubName) {
		for (UserClub clubList : data)
			if (clubList.getName().equalsIgnoreCase(clubName))
				return clubList.getName();
		return null;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(childResourceID, parent, false);
			holder.clubName = (TextView) convertView
					.findViewById(R.id.clubName);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		if (holder.clubName != null)
			holder.clubName.setText(data.get(position).getName());

		return convertView;
	}

	class ViewHolder {
		TextView clubName;
	}

}
