package com.qisoft.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.qisoft.view.fragment.ClubListFragment;
import com.qisoft.view.fragment.SettingFragment;

public class HomePageViewPager extends FragmentPagerAdapter {

	public HomePageViewPager(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getCount() {
		return 2;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return ClubListFragment.newInstance();
//		case 1:
//			return UserInforFragment.newInstance();
		default:
			return SettingFragment.newInstance();
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "Club";

//		case 1:
//			return "Me";

		case 1:
			return "Setting";
		}
		return super.getPageTitle(position);
	}

}
