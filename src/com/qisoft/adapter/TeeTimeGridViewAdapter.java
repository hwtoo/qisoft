package com.qisoft.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.teetime.TeeTimeList;

public class TeeTimeGridViewAdapter extends BaseAdapter {

	private LayoutInflater inflater = null;
	private List<TeeTimeList> data = new ArrayList<TeeTimeList>();
	private Context context = null;

	public TeeTimeGridViewAdapter(Context context, List<TeeTimeList> dataAry) {
		this.inflater = LayoutInflater.from(context);
		this.data = dataAry;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		viewHolder holder = null;
		if (convertView == null) {
			holder = new viewHolder();
			convertView = inflater.inflate(R.layout.teetime_gridview_child,
					parent, false);
			holder.teeTimeTextview = (TextView) convertView
					.findViewById(R.id.teetimeTextView);
			convertView.setTag(holder);
		} else
			holder = (viewHolder) convertView.getTag();

		TeeTimeList teeTimelistEntity = data.get(position);

		if (holder.teeTimeTextview != null)
			holder.teeTimeTextview.setText(teeTimelistEntity.getTeeTime());
		return convertView;
	}

	private class viewHolder {
		TextView teeTimeTextview;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

}
