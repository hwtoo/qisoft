package com.qisoft.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.qisoft.view.fragment.BookingFragment;
import com.qisoft.view.fragment.HistoryFragment;
import com.qisoft.view.fragment.MyBookingFragment;

public class TeeTimeViewPager extends FragmentPagerAdapter {

	public TeeTimeViewPager(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return MyBookingFragment.newInstance();
		case 1:
			return BookingFragment.newInstance();
		default:
			return HistoryFragment.newInstance();
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "My Booking";

		case 1:
			return "New";

		case 2:
			return "History";
		}
		return super.getPageTitle(position);
	}

	@Override
	public int getCount() {
		return 3;
	}
}
