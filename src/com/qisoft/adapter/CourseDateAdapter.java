package com.qisoft.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.clubCourseList.ClubCourseDateList;
import com.qisoft.xmlparse.entity.profileGet.DataClass;

public class CourseDateAdapter extends BaseAdapter {

	private List<ClubCourseDateList> data = new ArrayList<ClubCourseDateList>();
	private LayoutInflater inflater = null;

	public CourseDateAdapter(Context context, List<ClubCourseDateList> Array) {
		if (!data.isEmpty())
			data.clear();
		data = Array;
		inflater = LayoutInflater.from(context);
	}

	public void cleanUp() {
		data.clear();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	public String getDateName(int position) {
		return data.get(position).getDateName();
	}

	public String getDateValue(int position) {
		return data.get(position).getDateValue();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.spinner_child, parent,
					false);
			holder.spinnnerTitle = (TextView) convertView
					.findViewById(R.id.spinnnerTitle);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		if (holder.spinnnerTitle != null)
			holder.spinnnerTitle.setText(data.get(position).getDateName());

		return convertView;
	}

	class ViewHolder {
		TextView spinnnerTitle;
	}

}
