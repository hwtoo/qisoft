package com.qisoft.adapter;

import java.util.ArrayList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.viewbooking.TeeTimeViewEntity;

public class HistoryBookingRecyclerViewAdapter extends
		RecyclerView.Adapter<HistoryBookingRecyclerViewAdapter.ViewHolder> {

	private ArrayList<TeeTimeViewEntity> data = new ArrayList<TeeTimeViewEntity>();

	public HistoryBookingRecyclerViewAdapter() {
	}

	public void addData(TeeTimeViewEntity en) {
		data.add(en);
	}

	public boolean isEmpty() {
		return data.isEmpty();
	}

	public void clean() {
		data.clear();
	}

	@Override
	public int getItemCount() {
		return data.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		final TeeTimeViewEntity en = data.get(position);
		holder.bookingDateTextView.setText(": " + en.getDisplaydate());
		holder.bookingTeeTimeTextView.setText(": " + en.getDisplaytime());
		holder.bookingNumberTextView.setText(": " + en.getBookingid());

		if (en.getUsertrx() != null && en.getUsertrx().length() > 0
				&& !en.getUsertrx().equals("")) {
			holder.bookedByLinear.setVisibility(View.VISIBLE);
			holder.bookedByTextView.setText(": " + en.getUsertrx());
		} else
			holder.bookedByLinear.setVisibility(View.GONE);
		

	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.child_booking_history, parent, false);
		ViewHolder vh = new ViewHolder(v);
		return vh;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		public TextView bookingDateTextView = null,
				bookingTeeTimeTextView = null, bookingNumberTextView = null,
				bookedByTextView = null;
		public LinearLayout bookedByLinear = null;

		public ViewHolder(View v) {
			super(v);
			bookingDateTextView = (TextView) v
					.findViewById(R.id.bookingDateTextView);
			bookingTeeTimeTextView = (TextView) v
					.findViewById(R.id.bookingTeeTimeTextView);
			bookingNumberTextView = (TextView) v
					.findViewById(R.id.bookingNumberTextView);
			bookedByTextView = (TextView) v.findViewById(R.id.bookedByTextView);
			bookedByLinear = (LinearLayout) v.findViewById(R.id.bookedByLinear);
		}

	}
}
