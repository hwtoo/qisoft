package com.qisoft.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.qisoft.view.fragment.ClubListFragment;
import com.qisoft.view.fragment.ProfileFragment;
import com.qisoft.view.fragment.SettingFragment;
import com.qisoft.view.fragment.StatementFragment;
import com.qisoft.view.fragment.StatusFragment;
import com.qisoft.view.fragment.UserInforFragment;

public class MembershipPageAdapter extends FragmentPagerAdapter {

	public MembershipPageAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return StatusFragment.newInstance();
		case 1:
			return StatementFragment.newInstance();
		default:
			return ProfileFragment.newInstance();
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "Status";

		case 1:
			return "Statement";

		case 2:
			return "Profile";
		}
		return super.getPageTitle(position);
	}
}
