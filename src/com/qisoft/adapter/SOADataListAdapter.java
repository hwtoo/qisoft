package com.qisoft.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.profileGet.DataClass;
import com.qisoft.xmlparse.entity.soadatelist.ClubSOADateList;

public class SOADataListAdapter extends BaseAdapter {

	private List<ClubSOADateList> data = new ArrayList<ClubSOADateList>();
	private LayoutInflater inflater = null;

	public SOADataListAdapter(Context context, List<ClubSOADateList> Array) {
		if (!data.isEmpty())
			data.clear();
		data = Array;
		inflater = LayoutInflater.from(context);
	}

	public void cleanUp() {
		data.clear();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	public Date getTrxdate(int position) {
		return data.get(position).getTrxdate();
	}

	// public int getPositionById(int id) {
	// for (int i = 0; i < data.size(); i++) {
	// DataClass dataClass = data.get(i);
	// if (dataClass.getId() == id)
	// return i;
	// }
	// return 0;
	// }

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.spinner_child, parent,
					false);
			holder.spinnnerTitle = (TextView) convertView
					.findViewById(R.id.spinnnerTitle);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		if (holder.spinnnerTitle != null)
			holder.spinnnerTitle.setText(data.get(position).getDateList());

		return convertView;
	}

	class ViewHolder {
		TextView spinnnerTitle;
	}

}
