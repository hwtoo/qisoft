package com.qisoft.view;

import android.os.Bundle;
import android.transition.Visibility;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.UserClub;
import com.qisoft.xmlparse.entity.UserClubMenu;

public class MainMenu extends AbstractActivity implements OnClickListener {

	private Button sefBtn = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		UserClub selectedClub = qisoft.getSelectedClub();
		getSupportActionBar().setTitle(selectedClub.getName());

		Button memberAcount = (Button) findViewById(R.id.MemberAcountBtn);
		memberAcount.setOnClickListener(this);
		Button teeTimebtn = (Button) findViewById(R.id.TeeTimeBtn);
		teeTimebtn.setOnClickListener(this);
		Button anualReportBtn = (Button) findViewById(R.id.AnualReportBtn);
		anualReportBtn.setOnClickListener(this);

		sefBtn = (Button) findViewById(R.id.facilityBtn);
		sefBtn.setOnClickListener(this);

		if (!selectedClub.getOptionFlag(11, 12, 13))
			memberAcount.setVisibility(View.GONE);

		if (!selectedClub.getOptionFlag(16))
			teeTimebtn.setVisibility(View.GONE);

		// TODO: temporary off Sport and facelity
		// if (selectedClub.getOptionFlag(18))
		// sefBtn.setVisibility(View.VISIBLE);

		for (UserClubMenu menu : qisoft.getmUserEntity().getGdatMenu()
				.getNewDataSet()) {
			if (menu.getClubcd().equalsIgnoreCase(selectedClub.getClubcd())) {
				if (menu.getMenugroup().equalsIgnoreCase("Anual Report")) {
					anualReportBtn.setVisibility(View.VISIBLE);
				}
			}
		}

	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.MemberAcountBtn:
			openView(ViewName.MEMBERSHIP_VIEW, null);
			break;

		case R.id.TeeTimeBtn:
			openView(ViewName.TEETIME, null);
			break;

		case R.id.AnualReportBtn:
			openView(ViewName.ANNUAL_REPORT, null);
			break;

		case R.id.facilityBtn:
			break;

		default:
			break;
		}
	}

}
