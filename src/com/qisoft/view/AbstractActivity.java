package com.qisoft.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.qisoft.R;
import com.qisoft.engine.service.ServiceGateway;
import com.qisoft.view.dialog.NoConnectionDialog;
import com.qisoft.view.dialog.RegisterClubDialog;
import com.qisoft.xmlparse.onXmlParserCallBack;

public abstract class AbstractActivity extends ActionBarActivity implements
		onXmlParserCallBack, DialogInterface.OnClickListener {

	protected Toolbar toolbar;
	protected QisoftApplication qisoft = null;
	private FrameLayout contentLayout;
	private Bundle repeatParam = null;

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 0)
				openDialog(DialogName.NO_CONNECTION, AbstractActivity.this);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_abstract_page);
		toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
		setSupportActionBar(toolbar);
		contentLayout = (FrameLayout) findViewById(R.id.frameLayout);
		setHomeAsIndicator();
		if (getApplication() instanceof QisoftApplication)
			qisoft = (QisoftApplication) getApplication();
	}

	private void setHomeAsIndicator() {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	public void setContentView(int layoutResID) {
		contentLayout.addView(getLayoutInflater().inflate(layoutResID, null));
	}

	protected void getData(int contentID, onXmlParserCallBack xmlListener,
			Bundle params) {
		if (params != null)
			params.putInt("CONTENT_ID", contentID);
		repeatParam = params;
		new ServiceGateway(this).getData(xmlListener, repeatParam,
				params.getBoolean("LOADING", true));
	}

	public void openView(ViewName viewName, Bundle localBundle) {
		Intent mIntent = null;
		switch (viewName) {

		case SIGN_IN_PAGE:
			mIntent = new Intent(this, SignInView.class);
			break;

		case SIGN_UP_PAGE:
			mIntent = new Intent(this, SignupView.class);
			break;

		case FORGET_PASSWORD:
			mIntent = new Intent(this, ForgetPassword.class);
			break;

		case HOMEPAGE:
			mIntent = new Intent(this, HomeView.class);
			break;

		case MAIN_MENU:
			mIntent = new Intent(this, MainMenu.class);
			break;

		case MEMBERSHIP_VIEW:
			mIntent = new Intent(this, MembershipView.class);
			break;

		case TEETIME:
			mIntent = new Intent(this, TeeTimeView.class);
			break;

		case ACCOUNT_STATEMENT:
			mIntent = new Intent(this, StatementView.class);
			break;

		case ANNUAL_REPORT:
			mIntent = new Intent(this, AnnualReportView.class);
			break;

		default:
			break;
		}
		if (mIntent != null) {
			if (localBundle != null)
				mIntent.putExtras(localBundle);
			startActivity(mIntent);
		}
	}

	public void openDialog(DialogName dialogName,
			DialogInterface.OnClickListener dialogListener) {

		switch (dialogName) {
		case NO_CONNECTION:
			new NoConnectionDialog(this).show(dialogListener);
			break;

		case REGISTER_CLUB:
			new RegisterClubDialog(this, qisoft).show();
			break;

		default:
			break;
		}
	}

	public enum ViewName {
		SIGN_IN_PAGE, SIGN_UP_PAGE, HOMEPAGE, FORGET_PASSWORD, MAIN_MENU, MEMBERSHIP_VIEW, TEETIME, ACCOUNT_STATEMENT, ANNUAL_REPORT;
	}

	public enum DialogName {
		NO_CONNECTION, REGISTER_CLUB
	}

	@Override
	public void onConnectionError(String errorMsg) {
		if (errorMsg.equalsIgnoreCase("CONNECTION_ERROR")) {
			handler.sendEmptyMessage(0);
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
//		if (which == DialogInterface.BUTTON_POSITIVE)
//			getData(repeatParam.getInt("CONTENT_ID"), this, repeatParam);
	}
}
