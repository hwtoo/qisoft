package com.qisoft.view.dialog;

import java.util.ArrayList;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.RegisterClubListAdapter;
import com.qisoft.engine.service.ServiceGateway;
import com.qisoft.view.QisoftApplication;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.ClubList;
import com.qisoft.xmlparse.entity.StatusEntity;
import com.qisoft.xmlparse.entity.UserClubLIst;

public class RegisterClubDialog extends Dialog implements
		android.view.View.OnClickListener, onXmlParserCallBack {

	private Spinner clubList = null;
	private EditText membershipEditText = null;
	private ArrayList<String> clubListAry = new ArrayList<String>();
	// private ArrayAdapter<String> dataAdapter;
	private RegisterClubListAdapter dataAdapter;
	private Context context;
	private QisoftApplication qisoft = null;

	public RegisterClubDialog(Context context, QisoftApplication qisoft) {
		super(context);
		this.context = context;
		this.qisoft = qisoft;
		setContentView(R.layout.dialog_register_club);
		this.setTitle("Register Club");

		clubList = (Spinner) findViewById(R.id.spinnerClub);
		dataAdapter = new RegisterClubListAdapter(context,
				R.layout.spinner_register_club);
		// dataAdapter = new ArrayAdapter<String>(context,
		// R.layout.spinner_register_club, R.id.clubName, clubListAry);
		clubList.setAdapter(dataAdapter);

		membershipEditText = (EditText) findViewById(R.id.membershipNoEditText);
		findViewById(R.id.registerClubBtn).setOnClickListener(this);
		getClubListing();
	}

	private void getClubListing() {
		Bundle clubBundle = new Bundle();
		clubBundle.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		clubBundle.putString("sName", qisoft.getmUserEntity().getName());
		clubBundle.putBoolean("bFilterByLoginID", false);
		clubBundle.putString("sCheckSum", qisoft.getCheckSum());
		clubBundle.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		clubBundle.putString("sDeviceCD", qisoft.getDeviceId());
		clubBundle.putString("FragementName", getClass().getName());
		clubBundle.putInt("CONTENT_ID", 1004);
		new ServiceGateway(context).getData(this, clubBundle, true);
	}

	@Override
	public void onClick(View v) {
		String membershipText = membershipEditText.getText().toString();
		if (membershipText.length() > 0) {
			UserClubLIst selectedClub = (UserClubLIst) clubList
					.getSelectedItem();
			Bundle params = new Bundle();
			params.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
			params.putString("sMember", membershipText);
			params.putString("sClubCD", selectedClub.getCd());
			params.putString("sCheckSum", qisoft.getCheckSum());
			params.putString("sTokenCD", qisoft.getmUserEntity().getToken());
			params.putString("sDeviceCD", qisoft.getDeviceId());
			params.putInt("CONTENT_ID", 1005);
			new ServiceGateway(context).getData(this, params, true);
		} else {
			Toast.makeText(context, "Please key in Membership Number",
					Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		Serializer serializer = new Persister();
		try {
			if (contentID == 1004)
				return serializer.read(ClubList.class, result);
			else if (contentID == 1005)
				return serializer.read(StatusEntity.class, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (object instanceof ClubList && contentID == 1004) {
			ClubList userEntity = (ClubList) object;
			boolean isError = userEntity.getiStatus() != 0;

			if (isError) {
				Toast.makeText(context, userEntity.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				for (UserClubLIst clubList : userEntity.getdatParam1()
						.getNewDataSet()) {
					dataAdapter.addData(clubList);
				}
				dataAdapter.notifyDataSetChanged();
			}
		} else if (contentID == 1005 && object instanceof StatusEntity) {
			StatusEntity entity = (StatusEntity) object;
			boolean isError = entity.getiStatus() != 0;

			if (isError)
				Toast.makeText(context, entity.getsMsg(), Toast.LENGTH_SHORT)
						.show();

		}
	}

	@Override
	public void onConnectionError(String errorMsg) {
		// TODO Auto-generated method stub

	}

}
