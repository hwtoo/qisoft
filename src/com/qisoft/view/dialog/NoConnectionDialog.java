package com.qisoft.view.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.qisoft.R;

public class NoConnectionDialog extends AlertDialog {

	private Context context;

	public NoConnectionDialog(Context context) {
		super(context);
		this.context = context;
	}

	protected NoConnectionDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		this.context = context;
	}

	protected NoConnectionDialog(Context context, int theme) {
		super(context, android.R.style.Theme_Holo_Dialog);
		this.context = context;
	}

	public void show(DialogInterface.OnClickListener dialogListener) {
		setMessage(context.getString(R.string.noConnection));
		setButton(BUTTON_POSITIVE, "OK", dialogListener);
		show();
	}
}
