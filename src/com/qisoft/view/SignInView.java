package com.qisoft.view;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.UserEntity;

public class SignInView extends AbstractActivity implements OnClickListener {

	private EditText userIdEditText = null, passwordEditText = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().hide();
		setTitle("Sign In");
		setContentView(R.layout.activity_sign_in_page);
		userIdEditText = (EditText) findViewById(R.id.userIdEditText);
		passwordEditText = (EditText) findViewById(R.id.passEditText);
		findViewById(R.id.forgotPasswordTextView).setOnClickListener(this);
		findViewById(R.id.loginBtn).setOnClickListener(this);
		findViewById(R.id.registerTextView).setOnClickListener(this);

		if (qisoft.getUserEmail() != null && qisoft.getUserPassword() != null) {
			loginProcess(qisoft.getUserEmail(), qisoft.getUserPassword());
		}
	}

	private void loginProcess(String email, String password) {
		Bundle param = new Bundle();
		param.putString("email", email);
		param.putString("password", password);
		param.putString("deviceCD", qisoft.getDeviceId());
		param.putString("sCheckSum", qisoft.getCheckSum());
		getData(1000, this, param);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.registerTextView:
			// open Register Page
			openView(ViewName.SIGN_UP_PAGE, null);
			break;

		case R.id.loginBtn:
			// User Login:
			String userName = userIdEditText.getText().toString();
			String password = passwordEditText.getText().toString();
			if (password.equals("") || password.length() <= 0) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.insertPassword),
						Toast.LENGTH_SHORT).show();
				break;
			}
			loginProcess(userName, password);

			break;

		case R.id.forgotPasswordTextView:
			openView(ViewName.FORGET_PASSWORD, null);
			break;

		default:
			break;
		}
	}

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentId) {
		// TODO: simpleXML Code
		Serializer serializer = new Persister();
		try {
			return serializer.read(UserEntity.class, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentId) {
		if (object instanceof UserEntity) {
			UserEntity userEntity = (UserEntity) object;
			boolean isError = userEntity.getIsStatus() != 0;
			if (isError) {
				Toast.makeText(this, userEntity.getMsg(), Toast.LENGTH_SHORT)
						.show();
			} else {
				qisoft.storeEmailAndPassword(localParam.getString("email"),
						localParam.getString("password"));
				qisoft.setmUserEntity((UserEntity) object);
				openView(ViewName.HOMEPAGE, null);
				finish();
			}

		}

	}
}
