package com.qisoft.view;

import android.os.Bundle;
import android.widget.TextView;

import com.qisoft.R;

public class SplashView extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_page);
		getSupportActionBar().hide();

		TextView companyName = (TextView) findViewById(R.id.splashCompanyName1);
		companyName.postDelayed(Action, 2500L);
	}

	private Runnable Action = new Runnable() {
		public void run() {
			Bundle localBundle = getIntent().getExtras();
			// if (localBundle == null)
			// localBundle = new Bundle();
			// localBundle.putInt("PASS_OPTION", 1);

			// Intent mIntent = new Intent(SplashView.this, SignInView.class);
			// if (mIntent != null) {
			// if (localBundle != null)
			// mIntent.putExtras(localBundle);
			// startActivity(mIntent);
			// }
			// finish();
			openView(ViewName.SIGN_IN_PAGE, localBundle);
			finish();
		}
	};

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentId) {
		// TODO Auto-generated method stub

	}

}
