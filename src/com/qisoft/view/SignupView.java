package com.qisoft.view;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.util.Util;

public class SignupView extends AbstractActivity implements OnClickListener,
		OnDateSetListener {

	private EditText etLogin = null, etPassword = null,
			etConfirmPassword = null, etName = null, etDOB = null, etIC = null,
			etPassport = null, etEmail = null, etMobile = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Sign Up");
		setContentView(R.layout.activity_sign_up_page);

		etLogin = (EditText) findViewById(R.id.etLoginName);
		etPassword = (EditText) findViewById(R.id.etPassword);
		etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
		etName = (EditText) findViewById(R.id.etName);
		etDOB = (EditText) findViewById(R.id.etDOB);
		etDOB.setKeyListener(null);
		etDOB.setOnClickListener(this);
		etIC = (EditText) findViewById(R.id.etIcNo);
		etPassport = (EditText) findViewById(R.id.etPassportNo);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etMobile = (EditText) findViewById(R.id.etMobileNo);
		findViewById(R.id.registerBtn).setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		// setDOB();
	}

	// private void setDOB() {
	// Calendar newCalendar = Calendar.getInstance();
	// fromDatePickerDialog = new DatePickerDialog(this,
	// new OnDateSetListener() {
	// public void onDateSet(DatePicker view, int year,
	// int monthOfYear, int dayOfMonth) {
	// Calendar newDate = Calendar.getInstance();
	// newDate.set(year, monthOfYear, dayOfMonth);
	// etDOB.setText(Util.getDOBInText(newDate));
	// }
	// }, newCalendar.get(Calendar.YEAR),
	// newCalendar.get(Calendar.MONTH),
	// newCalendar.get(Calendar.DAY_OF_MONTH));
	// }

	private boolean checkPassword(String password, String confirmPassword) {
		boolean samePassword = password.equals(confirmPassword);
		if (!samePassword) {
			etPassword.setError("Password not match");
			etConfirmPassword.setError("Password not match");
			Toast.makeText(this, "Password not match", Toast.LENGTH_SHORT)
					.show();
		}
		return samePassword;
	}

	private boolean checkPassportOrIC(String passport, String Ic) {
		boolean pass = true;
		if ((passport.length() <= 0 || passport.equals(""))
				&& (Ic.length() <= 0 || Ic.equals(""))) {
			pass = false;
			etPassport
					.setError("IC No. or Passport No. either one must keyed.");
			etIC.setError("IC No. or Passport No. either one must keyed.");
			Toast.makeText(this,
					"IC No. or Passport No. either one must keyed.",
					Toast.LENGTH_SHORT).show();
		}
		return pass;
	}

	private void checkRequirement(EditText editText) {
		if (editText.length() <= 0 || editText.equals("")) {
			editText.setError("Field requirement");
		}
	}

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentId) {
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentId) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.etDOB:
			// fromDatePickerDialog.show();
			Util.getDateFromDatePicker(v.getId(), this, this, null, null);
			break;
		case R.id.registerBtn:
			String loginID = etLogin.getEditableText().toString();
			String password = etPassword.getEditableText().toString();
			String name = etName.getEditableText().toString();
			String DOB = etDOB.getEditableText().toString();
			String IC = etIC.getEditableText().toString();
			String passport = etPassport.getEditableText().toString();
			String email = etEmail.getEditableText().toString();
			String mobile = etMobile.getEditableText().toString();

			if ((loginID.length() > 0 && !loginID.equals(""))
					&& (password.length() > 0 && !password.equals(""))
					&& (name.length() > 0 && !name.equals(""))
					&& (DOB.length() > 0 && !DOB.equals(""))
					&& (mobile.length() > 0 && !mobile.equals(""))
					&& (email.length() > 0 && !email.equals(""))) {
				if (checkPassword(password, etConfirmPassword.getEditableText()
						.toString())) {
					if (checkPassportOrIC(passport, IC)) {
						Bundle registerBundle = new Bundle();
						registerBundle.putString("sLoginName", loginID);
						registerBundle.putString("sPassword", password);
						registerBundle.putString("sName", name);
						registerBundle.putString("dteDOB", DOB);
						registerBundle.putString("sIcNo", IC);
						registerBundle.putString("sPassportno", passport);
						registerBundle.putString("sEmail", email);
						registerBundle.putString("sPhoneHP", mobile);
						registerBundle.putString("deviceCD",
								qisoft.getDeviceId());
						registerBundle.putString("sCheckSum",
								qisoft.getCheckSum());
						getData(1001, this, registerBundle);
					}
				}
			} else {
				checkRequirement(etLogin);
				checkRequirement(etPassword);
				checkRequirement(etName);
				checkRequirement(etDOB);
				checkRequirement(etEmail);
				checkRequirement(etMobile);
				Toast.makeText(this, "Field is compulsory", Toast.LENGTH_SHORT)
						.show();
			}

			break;

		default:
			break;
		}

	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar newDate = Calendar.getInstance();
		newDate.set(year, monthOfYear, dayOfMonth);
		etDOB.setText(Util.getDOBInText(newDate));
	}

}
