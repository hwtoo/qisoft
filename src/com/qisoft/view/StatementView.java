package com.qisoft.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.SOADataListAdapter;
import com.qisoft.adapter.StatementAdapter;
import com.qisoft.util.DateFormatTransformer;
import com.qisoft.util.Util;
import com.qisoft.xmlparse.entity.soa.SOAEntity;
import com.qisoft.xmlparse.entity.soadatelist.SOADateLIstEntity;

public class StatementView extends AbstractActivity implements
		OnItemSelectedListener, OnScrollListener {

	private Spinner statementDate = null;
	private SOADataListAdapter adapter;
	private StatementAdapter statementAdatper = null;
	private ListView mListview = null;
	private TextView totalAmount = null;
	private LinearLayout listviewHeader = null;

	private boolean isAcountStatment = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		if (getIntent() != null && getIntent().getExtras() != null
				&& getIntent().getExtras().get("isAcountStatment") != null)
			isAcountStatment = getIntent().getExtras().getBoolean(
					"isAcountStatment");

		setContentView(R.layout.activity_statement);
		statementDate = (Spinner) findViewById(R.id.spinnerStatementDate);
		statementDate.setOnItemSelectedListener(this);

		listviewHeader = (LinearLayout) findViewById(R.id.listviewHeader);
		mListview = (ListView) findViewById(R.id.listViewID);
		if (isAcountStatment) {
			mListview.setOnScrollListener(this);
			View footer = getLayoutInflater().inflate(
					R.layout.footer_statement, null);
			totalAmount = (TextView) footer.findViewById(R.id.totalFooter);
			mListview.addFooterView(footer);
		}
		statementAdatper = new StatementAdapter(this, isAcountStatment);
		mListview.setAdapter(statementAdatper);

		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sClubCD", qisoft.getSelectedClub().getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putBoolean("LOADING", true);

		if (isAcountStatment) {
			actionBar.setTitle("Account Statement");
			getData(1016, this, param);

		}

		else {
			actionBar.setTitle("Bonus Point Statement");
			getData(1018, this, param);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		try {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			RegistryMatcher m = new RegistryMatcher();
			m.bind(Date.class, new DateFormatTransformer(format));

			Serializer serializer = new Persister(m);
			if (contentID == 1016 || contentID == 1018) {
				return serializer.read(SOADateLIstEntity.class, result);
			} else if (contentID == 1017 || contentID == 1019) {
				// Serializer serializer = new Persister();
				return serializer.read(SOAEntity.class, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (contentID == 1016 || contentID == 1018) {
			SOADateLIstEntity soaDateList = (SOADateLIstEntity) object;
			boolean isError = soaDateList.getiStatus() != 0;

			if (isError) {
				Toast.makeText(this, soaDateList.getsMsg(), Toast.LENGTH_SHORT)
						.show();
			} else {
				adapter = new SOADataListAdapter(this, soaDateList
						.getDatParam1().getClubSOADateList());
				statementDate.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
		} else if (contentID == 1017 || contentID == 1019) {
			SOAEntity soaEntity = (SOAEntity) object;
			boolean isError = soaEntity.getiStatus() != 0;

			if (isError) {
				Toast.makeText(this, soaEntity.getsMsg(), Toast.LENGTH_SHORT)
						.show();
			} else {
				statementAdatper.addData(soaEntity.getDatParam1()
						.getClubSOADateList());
				statementAdatper.notifyDataSetChanged();
				listviewHeader.setVisibility(View.VISIBLE);
				mListview.setVisibility(View.VISIBLE);
				mListview.setSelection(0);
			}

		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if (position != 0) {
			Date data = adapter.getTrxdate(position);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(data);
			Bundle param = new Bundle();
			param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
			param.putString("sClubCD", qisoft.getSelectedClub().getClubcd());
			param.putString("dteSoaDate", Util.getDOBInText(calendar));
			param.putString("sCheckSum", qisoft.getCheckSum());
			param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
			param.putString("sDeviceCD", qisoft.getDeviceId());
			param.putBoolean("LOADING", true);
			if (isAcountStatment)
				getData(1017, this, param);
			else
				getData(1019, this, param);

		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	private int preLast;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		final int lastItem = firstVisibleItem + visibleItemCount;
		if (lastItem == totalItemCount) {
			if (preLast != lastItem) {
				totalAmount.setText(Util.round(statementAdatper.getTotal()));
				preLast = lastItem;
			}
		}
	}

}
