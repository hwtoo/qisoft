package com.qisoft.view;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.qisoft.R;
import com.qisoft.view.slidingTab.SlidingTabDividerLayout;
import com.qisoft.xmlparse.onXmlParserCallBack;

public abstract class AbstractSlidingActivity extends AbstractActivity {
	protected SlidingTabDividerLayout slidingTabLayout = null;
	protected ViewPager viewPager = null;
	protected ArrayList<onXmlParserCallBack> fragementListner = new ArrayList<onXmlParserCallBack>();

	public abstract FragmentPagerAdapter getAdapter();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab_view);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		slidingTabLayout = (SlidingTabDividerLayout) findViewById(R.id.slidingTabLayout);
		slidingTabLayout.setDistributeEvenly(true);
		slidingTabLayout
				.setCustomTabColorizer(new SlidingTabDividerLayout.TabColorizer() {

					@Override
					public int getIndicatorColor(int position) {
						return getResources().getColor(R.color.colorPrimary);
					}
				});

		viewPager = (ViewPager) findViewById(R.id.viewPagerID);
		viewPager.setAdapter(getAdapter());

		slidingTabLayout.setViewPager(viewPager);
	}

	public void addFragmentListener(onXmlParserCallBack listener) {
		if (!fragementListner.contains(listener))
			this.fragementListner.add(listener);
	}

	public QisoftApplication getQisoftApplication() {
		return this.qisoft;
	}

	public void getData(int contentId, Bundle bundle) {
		getData(contentId, this, bundle);
	}

}
