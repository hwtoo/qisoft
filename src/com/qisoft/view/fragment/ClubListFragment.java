package com.qisoft.view.fragment;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.ClubListAdapter;
import com.qisoft.view.AbstractActivity.DialogName;
import com.qisoft.view.AbstractActivity.ViewName;
import com.qisoft.view.HomeView;
import com.qisoft.view.QisoftApplication;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.ClubList;
import com.qisoft.xmlparse.entity.UserClub;
import com.qisoft.xmlparse.entity.UserClubLIst;

public class ClubListFragment extends Fragment implements OnClickListener,
		OnItemClickListener, onXmlParserCallBack,
		DialogInterface.OnClickListener {

	private ListView clubListView = null;
	private RelativeLayout addClubFooter = null;
	private ClubListAdapter adapter = null;
	private QisoftApplication qisoft;

	// private HomeView homeView = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new ClubListAdapter(getActivity(), R.layout.child_clublist);
		if (getActivity() instanceof HomeView)
			qisoft = ((HomeView) getActivity()).getQisoftApplication();

		for (UserClub clubList : qisoft.getmUserEntity().getUserClubList())
			adapter.addData(clubList);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onResume() {
		super.onResume();
		((HomeView) getActivity()).getSupportActionBar().setTitle("Club");
	}

	private void getClubListing(boolean userClubList) {
		Bundle clubBundle = new Bundle();
		clubBundle.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		clubBundle.putString("sName", qisoft.getmUserEntity().getName());
		clubBundle.putBoolean("bFilterByLoginID", userClubList);
		clubBundle.putString("sCheckSum", qisoft.getCheckSum());
		clubBundle.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		clubBundle.putString("sDeviceCD", qisoft.getDeviceId());
		clubBundle.putString("FragementName", getClass().getName());
		((HomeView) getActivity()).getData(1004, clubBundle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_clublist, container,
				false);
		clubListView = (ListView) view.findViewById(R.id.clubListView);
		View footerView = inflater
				.inflate(R.layout.child_add_club_footer, null);
		addClubFooter = (RelativeLayout) footerView
				.findViewById(R.id.addClubFooter);
		addClubFooter.setOnClickListener(this);
		clubListView.setOnItemClickListener(this);
		clubListView.addFooterView(addClubFooter);
		clubListView.setAdapter(adapter);

		((HomeView) getActivity()).addFragmentListener(this);
		// if (adapter.isEmpty())
		// getClubListing(true);

		return view;
	}

	public static Fragment newInstance() {
		ClubListFragment about = new ClubListFragment();
		return about;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.addClubFooter) {
			((HomeView) getActivity()).openDialog(DialogName.REGISTER_CLUB,
					this);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		UserClub userClub = (UserClub) adapter.getItem(position);
		qisoft.setSelectedClub(userClub);
		((HomeView) getActivity()).openView(ViewName.MAIN_MENU, null);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		if (contentID == 1004) {
			Serializer serializer = new Persister();
			try {
				return serializer.read(ClubList.class, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (object instanceof ClubList) {
			ClubList userEntity = (ClubList) object;
			boolean isError = userEntity.getiStatus() != 0;

			if (isError) {
				Toast.makeText(getActivity(), userEntity.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				if (!adapter.isEmpty())
					adapter.cleanUp();
				for (UserClub clubList : qisoft.getmUserEntity().getGdatClub()
						.getNewDataSet())
					adapter.addData(clubList);
				adapter.notifyDataSetChanged();
			}

		}

		// for (UserClub userClub : ((HomeView) getActivity())
		// .getQisoftApplication().getmUserEntity().getUserClubList()) {
		// adapter.addData(userClub);
		// }
	}

	@Override
	public void onConnectionError(String errorMsg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub

	}
}
