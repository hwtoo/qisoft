package com.qisoft.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qisoft.view.HomeView;
import com.qisoft.view.QisoftApplication;
import com.qisoft.xmlparse.onXmlParserCallBack;

public class UserInforFragment extends Fragment implements onXmlParserCallBack {
	private QisoftApplication qisoft;

	public static Fragment newInstance() {
		UserInforFragment statement = new UserInforFragment();
		return statement;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof HomeView)
			qisoft = ((HomeView) getActivity()).getQisoftApplication();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		TextView textView = new TextView(getActivity());
		textView.setText(getClass().getName());
		return textView;
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {

	}

	@Override
	public void onConnectionError(String errorMsg) {

	}
}