package com.qisoft.view.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.SpinnerAdapter;
import com.qisoft.util.DateFormatTransformer;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.util.Util;
import com.qisoft.view.MembershipView;
import com.qisoft.view.QisoftApplication;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.StatusEntity;
import com.qisoft.xmlparse.entity.UserClub;
import com.qisoft.xmlparse.entity.accountStatus.AccountStatusEntity;
import com.qisoft.xmlparse.entity.profileGet.ClubUpdateProfileGet;
import com.qisoft.xmlparse.entity.profileGet.DataClass;
import com.qisoft.xmlparse.entity.profileGet.ProfileGetEntity;

public class ProfileFragment extends Fragment implements onXmlParserCallBack,
		OnClickListener {

	private QisoftApplication qisoft;
	private MembershipView activity = null;
	public static String fragmentName = "";
	private boolean editableFlag = false;

	private Button updateProfileBtn = null;
	// , btnMale = null, btnFemale = null;

	private EditText editTextMember = null, editTextName = null,
			editTextDOB = null, editTextIC = null, editTextPassport = null,
			editTextTel = null, editTextHp = null, editTextEmail = null,
			editTextAddress = null, editTextPostcode = null;
	private EditText[] editTextAry = new EditText[10];

	private Spinner spinnerSalutation = null, spinnerNationality = null,
			spinnerRace = null, spinnerReligion = null, spinnerCity = null,
			spinnerState = null, spinnerCountry = null, spinnerGender = null;
	private Spinner[] spinnerAry = new Spinner[8];

	private UserClub club = null;

	public static Fragment newInstance() {
		ProfileFragment statement = new ProfileFragment();
		ProfileFragment.fragmentName = statement.getClass().getName();
		return statement;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof MembershipView) {
			activity = (MembershipView) getActivity();
			qisoft = activity.getQisoftApplication();
			club = qisoft.getSelectedClub();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_profile, container,
				false);
		TextView title;
		title = (TextView) view.findViewById(R.id.memberNumberLayout)
				.findViewById(R.id.statusTitle);
		title.setText("Member No");
		editTextMember = (EditText) view.findViewById(R.id.memberNumberLayout)
				.findViewById(R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.memberNameLayout)
				.findViewById(R.id.statusTitle);
		title.setText("Name");
		editTextName = (EditText) view.findViewById(R.id.memberNameLayout)
				.findViewById(R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.dobLayout).findViewById(
				R.id.statusTitle);
		title.setText("D.O.B");
		editTextDOB = (EditText) view.findViewById(R.id.dobLayout)
				.findViewById(R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.icLayout).findViewById(
				R.id.statusTitle);
		title.setText("IC");
		editTextIC = (EditText) view.findViewById(R.id.icLayout).findViewById(
				R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.passportLayout).findViewById(
				R.id.statusTitle);
		title.setText("Passport");
		editTextPassport = (EditText) view.findViewById(R.id.passportLayout)
				.findViewById(R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.telLayout).findViewById(
				R.id.statusTitle);
		title.setText("Tel No");
		editTextTel = (EditText) view.findViewById(R.id.telLayout)
				.findViewById(R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.hpLayout).findViewById(
				R.id.statusTitle);
		title.setText("Handphone");
		editTextHp = (EditText) view.findViewById(R.id.hpLayout).findViewById(
				R.id.statusEditText);

		title = (TextView) view.findViewById(R.id.emailLayout).findViewById(
				R.id.statusTitle);
		title.setText("Email");
		editTextEmail = (EditText) view.findViewById(R.id.emailLayout)
				.findViewById(R.id.statusEditText);

		// title = (TextView)
		// view.findViewById(R.id.addressLayout).findViewById(
		// R.id.statusTitle);
		// title.setText("Address");
		// view.findViewById(R.id.addressLayout).findViewById(R.id.statusEditText)
		// .setVisibility(View.INVISIBLE);

		editTextAddress = (EditText) view.findViewById(R.id.editTextAddress);

		title = (TextView) view.findViewById(R.id.postcodeLayout).findViewById(
				R.id.statusTitle);
		title.setText("Postcode");
		editTextPostcode = (EditText) view.findViewById(R.id.postcodeLayout)
				.findViewById(R.id.statusEditText);

		editTextAry[0] = editTextMember;
		editTextAry[1] = editTextName;
		editTextAry[2] = editTextDOB;
		editTextAry[3] = editTextIC;
		editTextAry[4] = editTextPassport;
		editTextAry[5] = editTextTel;
		editTextAry[6] = editTextHp;
		editTextAry[7] = editTextEmail;
		editTextAry[8] = editTextAddress;
		editTextAry[9] = editTextPostcode;

		title = (TextView) view.findViewById(R.id.saluationLayout)
				.findViewById(R.id.statusTitle);
		title.setText("Saluation");
		spinnerSalutation = (Spinner) view.findViewById(R.id.saluationLayout)
				.findViewById(R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.nationalityLayout)
				.findViewById(R.id.statusTitle);
		title.setText("Nationality");
		spinnerNationality = (Spinner) view
				.findViewById(R.id.nationalityLayout).findViewById(
						R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.raceLayout).findViewById(
				R.id.statusTitle);
		title.setText("Race");
		spinnerRace = (Spinner) view.findViewById(R.id.raceLayout)
				.findViewById(R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.religionLayout).findViewById(
				R.id.statusTitle);
		title.setText("Race");
		spinnerReligion = (Spinner) view.findViewById(R.id.religionLayout)
				.findViewById(R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.cityLayout).findViewById(
				R.id.statusTitle);
		title.setText("City");
		spinnerCity = (Spinner) view.findViewById(R.id.cityLayout)
				.findViewById(R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.stateLayout).findViewById(
				R.id.statusTitle);
		title.setText("State");
		spinnerState = (Spinner) view.findViewById(R.id.stateLayout)
				.findViewById(R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.countryLayout).findViewById(
				R.id.statusTitle);
		title.setText("Country");
		spinnerCountry = (Spinner) view.findViewById(R.id.countryLayout)
				.findViewById(R.id.statusSpinner);

		title = (TextView) view.findViewById(R.id.genderLayout).findViewById(
				R.id.statusTitle);
		title.setText("Gender");
		spinnerGender = (Spinner) view.findViewById(R.id.genderLayout)
				.findViewById(R.id.statusSpinner);

		spinnerAry[0] = spinnerSalutation;
		spinnerAry[1] = spinnerNationality;
		spinnerAry[2] = spinnerRace;
		spinnerAry[3] = spinnerReligion;
		spinnerAry[4] = spinnerCity;
		spinnerAry[5] = spinnerState;
		spinnerAry[6] = spinnerCountry;
		spinnerAry[7] = spinnerGender;

		updateProfileBtn = (Button) view.findViewById(R.id.updateProfileBtn);
		updateProfileBtn.setOnClickListener(this);

		// spinnerGender = (Spinner) view.findViewById(R.id.genderSpinner);

		// btnMale = (Button) view.findViewById(R.id.btnMale);
		// btnMale.setOnClickListener(this);
		// btnMale.setSelected(true);
		// btnFemale = (Button) view.findViewById(R.id.btnFemale);
		// btnFemale.setOnClickListener(this);
		// btnFemale.setSelected(false);

		changeFlag(false, false);

		getProfile();

		return view;
	}

	private void getProfile() {
		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sClubCD", club.getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putString("FragmentName", ProfileFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.GET_PROFILE.toString());
		activity.getFragmentData(param, this, 1008);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		try {
			if (contentID == 1008) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

				RegistryMatcher m = new RegistryMatcher();
				m.bind(Date.class, new DateFormatTransformer(format));

				Serializer serializer = new Persister(m);

				return serializer.read(ProfileGetEntity.class, result);

			} else if (contentID == 1015) {
				Serializer serializer2 = new Persister();
				return serializer2.read(StatusEntity.class, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (object instanceof ProfileGetEntity) {
			ProfileGetEntity profileGetEntity = (ProfileGetEntity) object;
			boolean isError = profileGetEntity.getiStatus() != 0;

			if (isError) {
				Toast.makeText(getActivity(), profileGetEntity.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				Message msg = new Message();
				msg.what=0;
				msg.obj = profileGetEntity;
				handler.sendMessage(msg);
//				initData(profileGetEntity);
			}
		} else if (contentID == 1015 && object instanceof StatusEntity) {
			StatusEntity mStatusEntity = (StatusEntity) object;
			Toast.makeText(getActivity(), mStatusEntity.getsMsg(),
					Toast.LENGTH_SHORT).show();
			if (editableFlag) {
				editableFlag = false;
				changeFlag(editableFlag, false);
			}
		}
	}
	
	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			initData((ProfileGetEntity)msg.obj);
		}
	};
	

	// ProfileGetEntity profileGetEntity;

	private void initData(ProfileGetEntity profileGetEntity) {
		ClubUpdateProfileGet userProfile = profileGetEntity.getDatParam1()
				.getUserProfile();
		editTextMember.setText(userProfile.getCd());
		editTextName.setText(userProfile.getFullname());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(userProfile.getDob());
		editTextDOB.setText(Util.getDOBInText(calendar));
		editTextIC.setText(userProfile.getIcno());
		editTextPassport.setText(userProfile.getPassportno());
		editTextTel.setText(userProfile.getHousephone());
		editTextHp.setText(userProfile.getHandphone());
		editTextEmail.setText(userProfile.getEmailaddress());
		editTextAddress.setText(userProfile.getMailaddress());
		editTextPostcode.setText(userProfile.getMailpostcode());

		// boolean isMale = userProfile.getGender().equalsIgnoreCase("M") ? true
		// : false;
		// btnMale.setSelected(isMale);
		// btnFemale.setSelected(!isMale);

		initSpinner(userProfile, profileGetEntity);
	}

	private void initSpinner(ClubUpdateProfileGet userProfile,
			ProfileGetEntity profileGetEntity) {

		SpinnerAdapter mSpinnerAdapter = new SpinnerAdapter(getActivity(),
				profileGetEntity.getDatParam2().getNewDataSet());
		spinnerSalutation.setAdapter(mSpinnerAdapter);
		spinnerSalutation.setSelection(mSpinnerAdapter
				.getPositionById(userProfile.getSalutationid()));

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), profileGetEntity
				.getDatParam3().getNewDataSet());
		spinnerNationality.setAdapter(mSpinnerAdapter);
		spinnerNationality.setSelection(mSpinnerAdapter
				.getPositionById(userProfile.getNationalityid()));

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), profileGetEntity
				.getDatParam4().getNewDataSet());
		spinnerRace.setAdapter(mSpinnerAdapter);
		spinnerRace.setSelection(mSpinnerAdapter.getPositionById(userProfile
				.getRaceid()));

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), profileGetEntity
				.getDatParam5().getNewDataSet());
		spinnerReligion.setAdapter(mSpinnerAdapter);
		spinnerReligion.setSelection(mSpinnerAdapter
				.getPositionById(userProfile.getReligionid()));

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), profileGetEntity
				.getDatParam6().getNewDataSet());
		spinnerCity.setAdapter(mSpinnerAdapter);
		spinnerCity.setSelection(mSpinnerAdapter.getPositionById(userProfile
				.getMailcityid()));

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), profileGetEntity
				.getDatParam7().getNewDataSet());
		spinnerState.setAdapter(mSpinnerAdapter);
		spinnerState.setSelection(mSpinnerAdapter.getPositionById(userProfile
				.getMailstateid()));

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), profileGetEntity
				.getDatParam8().getNewDataSet());
		spinnerCountry.setAdapter(mSpinnerAdapter);
		spinnerCountry.setSelection(mSpinnerAdapter.getPositionById(userProfile
				.getMailcountryid()));

		List<DataClass> genderData = new ArrayList<DataClass>();
		DataClass male = new DataClass();
		male.setDescs("Male");
		male.setGender("M");
		genderData.add(male);

		DataClass female = new DataClass();
		female.setDescs("Female");
		female.setGender("F");
		genderData.add(female);

		mSpinnerAdapter = new SpinnerAdapter(getActivity(), genderData);
		spinnerGender.setAdapter(mSpinnerAdapter);

		if (userProfile.getGender().equalsIgnoreCase("M") ? true : false)
			spinnerGender.setSelection(0);
		else
			spinnerGender.setSelection(1);

		// spinnerCountry.setSelection(mSpinnerAdapter.getPositionById(userProfile
		// .getMailcountryid()));

	}

	@Override
	public void onConnectionError(String errorMsg) {

	}

	private void submitChange() {
		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sClubCD", club.getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sFullName", editTextName.getText().toString());
		// param.putString("sGender", btnMale.isSelected() ? "M" : "F");
		// param.putString("sGender",
		// spinnerGender.getSelectedItemPosition() == 0 ? "M" : "F");
		param.putString("sGender", "M");
		param.putString("sDoB", editTextDOB.getText().toString());
		param.putString("sICNo", editTextIC.getText().toString());
		param.putString("sPassport", editTextPassport.getText().toString());
		param.putString("sHousePhone", editTextTel.getText().toString());
		param.putString("sHandPhone", editTextHp.getText().toString());
		param.putString("sEmail", editTextEmail.getText().toString());
		param.putString("sMailAddress", editTextAddress.getText().toString());
		param.putString("sMailPostCode", editTextPostcode.getText().toString());
		param.putString("lSalutation",
				String.valueOf(spinnerSalutation.getSelectedItemId()));
		param.putString("lNationality",
				String.valueOf(spinnerNationality.getSelectedItemId()));
		param.putString("lRace",
				String.valueOf(spinnerRace.getSelectedItemId()));
		param.putString("lReligion",
				String.valueOf(spinnerReligion.getSelectedItemId()));
		param.putString("lMailCity",
				String.valueOf(spinnerCity.getSelectedItemId()));
		param.putString("lMailState",
				String.valueOf(spinnerState.getSelectedItemId()));
		param.putString("lMailCountry",
				String.valueOf(spinnerCountry.getSelectedItemId()));
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putString("FragmentName", ProfileFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.SET_PROFILE.toString());
		activity.getFragmentData(param, this, 1015);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.updateProfileBtn:
			if (!editableFlag) {
				editableFlag = true;
				changeFlag(editableFlag, false);
			} else {
				submitChange();
			}
			break;

		// case R.id.btnFemale:
		// case R.id.btnMale:
		// if (editableFlag) {
		// btnMale.setSelected(v.getId() == R.id.btnMale ? true : false);
		// btnFemale.setSelected(v.getId() == R.id.btnFemale ? true
		// : false);
		// }
		// break;

		default:
			break;
		}
	}

	@Override
	public void onDestroyView() {
		if (editableFlag)
			editableFlag = false;
		super.onDestroyView();
	}

	public boolean getEditableFlag() {
		return this.editableFlag;
	}

	public void changeFlag(boolean flag, boolean fromActivity) {
		if (fromActivity)
			editableFlag = flag;

		if (flag) {
			updateProfileBtn.setText("Submit");
		} else {
			updateProfileBtn.setText("Update");
		}
		for (EditText editText : editTextAry) {
			editText.setEnabled(flag);
			if (editText == editTextAddress)
				if (flag)
					editText.setBackground(getResources().getDrawable(
							android.R.drawable.editbox_background));
				else
					editText.setBackground(null);

		}
		for (Spinner spinner : spinnerAry)
			spinner.setEnabled(flag);

		// spinnerGender.setEnabled(flag);
	}
}