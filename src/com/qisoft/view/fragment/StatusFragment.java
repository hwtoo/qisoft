package com.qisoft.view.fragment;

import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.graphics.Color;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.view.MembershipView;
import com.qisoft.view.QisoftApplication;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.UserClub;
import com.qisoft.xmlparse.entity.accountStatus.AccountStatusEntity;
import com.qisoft.xmlparse.entity.accountStatus.ClubAccountStatus;

public class StatusFragment extends Fragment implements onXmlParserCallBack,
		OnItemClickListener {

	private QisoftApplication qisoft;
	public static String fragmentName;

	private TableLayout supplementTable = null;
	private TextView memberTextView = null, fullNameTextView = null,
			memberTypeTextView = null, statusTextView = null,
			signLimitTextView = null, aging1 = null, aging2 = null,
			aging3 = null, aging4 = null, aging5 = null,
			outStandingTextView = null;

	private UserClub club = null;
	private MembershipView activity = null;

	public static Fragment newInstance() {
		StatusFragment status = new StatusFragment();
		StatusFragment.fragmentName = status.getClass().getName();
		return status;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		memberTextView = null;
		fullNameTextView = null;
		memberTypeTextView = null;
		statusTextView = null;
		signLimitTextView = null;
		aging1 = null;
		aging2 = null;
		aging3 = null;
		aging4 = null;
		aging5 = null;
		outStandingTextView = null;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof MembershipView) {
			activity = (MembershipView) getActivity();
			qisoft = activity.getQisoftApplication();
		}

		if (qisoft != null)
			club = qisoft.getSelectedClub();
		activity.getSupportActionBar().setTitle(club.getName());
	}

	@Override
	public void onResume() {
		super.onResume();
		// activity.addFragmentListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_status, container, false);

		TextView title;
		title = (TextView) v.findViewById(R.id.memberLayout).findViewById(
				R.id.statusTitle);
		title.setText("Member#");
		memberTextView = (TextView) v.findViewById(R.id.memberLayout)
				.findViewById(R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.fullNameLayout).findViewById(
				R.id.statusTitle);
		title.setText("Full Name");
		fullNameTextView = (TextView) v.findViewById(R.id.fullNameLayout)
				.findViewById(R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.memberTypeLayout).findViewById(
				R.id.statusTitle);
		title.setText("Membership Type");
		memberTypeTextView = (TextView) v.findViewById(R.id.memberTypeLayout)
				.findViewById(R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.statusLayout).findViewById(
				R.id.statusTitle);
		title.setText("Status");
		statusTextView = (TextView) v.findViewById(R.id.statusLayout)
				.findViewById(R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.signingLayout).findViewById(
				R.id.statusTitle);
		title.setText("Signing Limit");
		signLimitTextView = (TextView) v.findViewById(R.id.signingLayout)
				.findViewById(R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.aging0Layout).findViewById(
				R.id.statusTitle);
		title.setText("Aging 0-30");
		aging1 = (TextView) v.findViewById(R.id.aging0Layout).findViewById(
				R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.aging1Layout).findViewById(
				R.id.statusTitle);
		title.setText("Aging 31-60");
		aging2 = (TextView) v.findViewById(R.id.aging1Layout).findViewById(
				R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.aging2Layout).findViewById(
				R.id.statusTitle);
		title.setText("Aging 61-90");
		aging3 = (TextView) v.findViewById(R.id.aging2Layout).findViewById(
				R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.aging3Layout).findViewById(
				R.id.statusTitle);
		title.setText("Aging 91-120");
		aging4 = (TextView) v.findViewById(R.id.aging3Layout).findViewById(
				R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.aging4Layout).findViewById(
				R.id.statusTitle);
		title.setText("Aging >121");
		aging5 = (TextView) v.findViewById(R.id.aging4Layout).findViewById(
				R.id.statusTextView);

		title = (TextView) v.findViewById(R.id.totalLayout).findViewById(
				R.id.statusTitle);
		title.setText("Total Outstanding");
		outStandingTextView = (TextView) v.findViewById(R.id.totalLayout)
				.findViewById(R.id.statusTextView);

		supplementTable = (TableLayout) v.findViewById(R.id.supplementTable);
		getAccountStatus();
		return v;
	}

	private void getAccountStatus() {
		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sClubCD", club.getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putString("FragmentName", StatusFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.GET_STATUS.toString());
		activity.getFragmentData(param, this, 1007);
		// activity.getData(1007, param);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		if (contentID == 1007) {
			Serializer serializer = new Persister();
			try {
				return serializer.read(AccountStatusEntity.class, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (object instanceof AccountStatusEntity) {
			AccountStatusEntity accountStatus = (AccountStatusEntity) object;
			boolean isError = accountStatus.getiStatus() != 0;

			if (isError) {
				Toast.makeText(getActivity(), accountStatus.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				Message msg = new Message();
				msg.what = 0;
				msg.obj = accountStatus;
				handler.sendMessage(msg);
				// initAccountData(accountStatus);
			}
		}
	}

	private void initAccountData(AccountStatusEntity accountStatus) {
		List<ClubAccountStatus> accountAry = accountStatus.getdatParam1()
				.getNewDataSet();
		if (accountAry.size() > 0) {
			initMemberStatus(accountAry.get(0));

			for (ClubAccountStatus account : accountAry)
				supplementTable.addView(initSupplementMember(account));
		}
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			initAccountData((AccountStatusEntity)msg.obj);
		}
	};

	private void initMemberStatus(ClubAccountStatus memberAccount) {
		memberTextView.setText(memberAccount.getCd());
		fullNameTextView.setText(memberAccount.getFullname());
		memberTypeTextView.setText(memberAccount.getMembertype());
		statusTextView.setText(memberAccount.getStatusdescs());
		signLimitTextView.setText(String.valueOf(memberAccount.getSignlimit()));
		aging1.setText(String.valueOf(memberAccount.getAging0()
				+ memberAccount.getAging1()));
		aging2.setText(memberAccount.getAging2() + "");
		aging3.setText(memberAccount.getAging3() + "");
		aging4.setText(memberAccount.getAging4() + "");
		aging5.setText(memberAccount.getAgingsum5() + "");
		outStandingTextView.setText(String.valueOf(memberAccount.getSignamt()));
	}

	private TableRow initSupplementMember(ClubAccountStatus memberAccount) {
		LayoutParams rowLayoutParam = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		LayoutParams textViewLayout = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		textViewLayout.setMargins(1, 1, 1, 1);

		TableRow tableRow = new TableRow(getActivity());
		tableRow.setBackgroundColor(Color.BLACK);
		tableRow.setLayoutParams(rowLayoutParam);

		TextView memberNo = new TextView(getActivity());
		memberNo.setLayoutParams(textViewLayout);
		memberNo.setGravity(Gravity.CENTER);
		memberNo.setBackgroundColor(getActivity().getResources().getColor(
				R.color.white));
		memberNo.setText(memberAccount.getCd());
		tableRow.addView(memberNo);

		TextView name = new TextView(getActivity());
		name.setLayoutParams(textViewLayout);
		name.setGravity(Gravity.CENTER);
		name.setBackgroundColor(getActivity().getResources().getColor(
				R.color.white));
		name.setText(memberAccount.getFullname());
		tableRow.addView(name);

		TextView status = new TextView(getActivity());
		status.setGravity(Gravity.CENTER);
		status.setLayoutParams(textViewLayout);
		status.setBackgroundColor(getActivity().getResources().getColor(
				R.color.white));
		status.setText(memberAccount.getStatusdescs());
		tableRow.addView(status);

		return tableRow;
	}

	@Override
	public void onConnectionError(String errorMsg) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

	}

}
