package com.qisoft.view.fragment;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.view.HomeView;
import com.qisoft.view.QisoftApplication;
import com.qisoft.view.AbstractActivity.ViewName;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.ClubList;
import com.qisoft.xmlparse.entity.StatusEntity;
import com.qisoft.xmlparse.entity.UserClub;

public class SettingFragment extends Fragment implements OnClickListener,
		onXmlParserCallBack {

	private QisoftApplication qisoft;

	public static Fragment newInstance() {
		SettingFragment fragment = new SettingFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		qisoft = ((HomeView) getActivity()).getQisoftApplication();
	}

	@Override
	public void onResume() {
		super.onResume();
		((HomeView) getActivity()).getSupportActionBar().setTitle("Setting");
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_setting, container, false);
		v.findViewById(R.id.logoutBtn).setOnClickListener(this);

		((HomeView) getActivity()).addFragmentListener(this);
		return v;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.logoutBtn) {
			Bundle param = new Bundle();
			param.putString("sLoginName", qisoft.getmUserEntity().getgsCD());
			param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
			param.putString("sDeviceCD", qisoft.getDeviceId());
			param.putString("sCheckSum", qisoft.getCheckSum());
			param.putString("FragementName", getClass().getName());
			((HomeView) getActivity()).getData(1006, param);
		}
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		if (contentID == 1006) {
			Serializer serializer = new Persister();
			try {
				return serializer.read(StatusEntity.class, result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (object instanceof StatusEntity) {
			StatusEntity mStatusEntity = (StatusEntity) object;
			boolean isError = mStatusEntity.getiStatus() != 0;

			if (!isError) {
				((HomeView) getActivity())
						.openView(ViewName.SIGN_IN_PAGE, null);
				((HomeView) getActivity()).finish();
			}
			Toast.makeText(getActivity(), mStatusEntity.getsMsg(),
					Toast.LENGTH_SHORT).show();
			qisoft.logoutUser();
		}
	}

	@Override
	public void onConnectionError(String errorMsg) {

	}
}
