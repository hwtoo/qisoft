package com.qisoft.view.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.view.AbstractActivity.ViewName;
import com.qisoft.view.MembershipView;
import com.qisoft.view.QisoftApplication;
import com.qisoft.xmlparse.entity.UserClub;

public class StatementFragment extends Fragment implements OnItemClickListener {
	private ListView mListview = null;
	private ArrayList<String> data = new ArrayList<String>();
	private MembershipView activity = null;
	private QisoftApplication qisoft;

	private final String STATEMENT = "Account Statement",
			BONUS_POINT = "Bonus Point Statement";

	public static Fragment newInstance() {
		StatementFragment statement = new StatementFragment();
		return statement;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof MembershipView) {
			activity = (MembershipView) getActivity();
			qisoft = activity.getQisoftApplication();
		}

		if (qisoft != null)
			activity.getSupportActionBar().setTitle(
					qisoft.getSelectedClub().getName());

		UserClub selectedClub = qisoft.getSelectedClub();
		if (selectedClub.getOptionFlag(11))// View Account Statement
			data.add(STATEMENT);
		if (selectedClub.getOptionFlag(17))// View Bonus Point Statement
			data.add(BONUS_POINT);

	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_clublist, container, false);
		mListview = (ListView) v.findViewById(R.id.clubListView);
		mListview.setAdapter(new statementAdapter(getActivity()));
		mListview.setOnItemClickListener(this);
		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Object obj = parent.getItemAtPosition(position);
		if (obj instanceof String) {
			String selectedStatement = (String) obj;
			Bundle bundler = new Bundle();
			bundler.putBoolean("isAcountStatment", selectedStatement
					.equalsIgnoreCase(STATEMENT) ? true : false);

//			if (selectedStatement.equalsIgnoreCase(STATEMENT))
				activity.openView(ViewName.ACCOUNT_STATEMENT, bundler);
//			else if (selectedStatement.equalsIgnoreCase(BONUS_POINT))
//				activity.openView(ViewName.BONUS_POINT_STATEMENT, bundler);
		}
	}

	private float total;

	private TableRow generateStatmentRow() {
		TableRow row = new TableRow(activity);

		LinearLayout linear = new LinearLayout(activity);
		linear.setOrientation(LinearLayout.VERTICAL);
		linear.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		TextView ref = new TextView(activity);
		ref.setText("06052015");
		TextView date = new TextView(activity);
		date.setText("12/06/2014");
		linear.addView(ref);
		linear.addView(date);
		row.addView(linear);

		TextView desc = new TextView(activity);
		desc.setText("DESCRIPTION");
		row.addView(desc);

		int amount = 120032492;
		TextView amountText = new TextView(activity);
		amountText.setText(String.valueOf(amount));
		row.addView(amountText);

		total = total + amount;
		TextView total = new TextView(activity);
		desc.setText(String.valueOf(total));
		row.addView(total);

		return row;
	}

	private class statementAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;

		public statementAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.child_clublist, parent,
						false);
				holder.statementType = (TextView) convertView
						.findViewById(R.id.clubName);
				holder.nextIcon = (ImageView) convertView
						.findViewById(R.id.nextIcon);
				convertView.setTag(holder);
			} else
				holder = (ViewHolder) convertView.getTag();

			if (holder.statementType != null) {
				holder.statementType.setText(data.get(position));
				holder.statementType.setTypeface(Typeface.DEFAULT);
			}
			holder.nextIcon.setVisibility(View.GONE);

			return convertView;
		}

		class ViewHolder {
			TextView statementType;
			ImageView nextIcon;
		}
	}
}