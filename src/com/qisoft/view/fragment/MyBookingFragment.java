package com.qisoft.view.fragment;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.ViewBookingRecyclerViewAdapter;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.util.onCardClickListener;
import com.qisoft.view.QisoftApplication;
import com.qisoft.view.TeeTimeView;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.StatusEntity;
import com.qisoft.xmlparse.entity.viewbooking.TeeTimeViewEntity;
import com.qisoft.xmlparse.entity.viewbooking.ViewBookingEntity;

public class MyBookingFragment extends Fragment implements onXmlParserCallBack,
		OnRefreshListener, onCardClickListener {

	private QisoftApplication qisoft;
	private TeeTimeView activity;
	public static String fragmentName;

	private SwipeRefreshLayout swipeRefreshLayout = null;
	private RecyclerView mRecyclerView = null;
	private ViewBookingRecyclerViewAdapter adapter;

	private String bookingDeletedNumber = "";

	public static Fragment newInstance() {
		MyBookingFragment status = new MyBookingFragment();
		MyBookingFragment.fragmentName = status.getClass().getName();
		return status;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof TeeTimeView) {
			activity = (TeeTimeView) getActivity();
			qisoft = activity.getQisoftApplication();
		}
		activity.getSupportActionBar().setTitle("View Booking");
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_viewbooking, container,
				false);
		swipeRefreshLayout = (SwipeRefreshLayout) v
				.findViewById(R.id.swipeLayout);
		swipeRefreshLayout.setOnRefreshListener(this);
		mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.setHasFixedSize(true);
		adapter = new ViewBookingRecyclerViewAdapter(this);
		mRecyclerView.setAdapter(adapter);

		getBooking(activity.getRefreshViewBookingList(), true);
		activity.resetRefreshViewBookingList();
		return v;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser && activity.getRefreshViewBookingList()) {
			getBooking(activity.getRefreshViewBookingList(), true);
			activity.resetRefreshViewBookingList();
		}
	}

	private void getBooking(boolean refreshData, boolean loading) {
		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sClubCD", qisoft.getSelectedClub().getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putBoolean("RefreshData", refreshData);
		param.putBoolean("LOADING", loading);
		param.putString("FragmentName", MyBookingFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.VIEW_BOOKING.toString());
		activity.getFragmentData(param, this, 1012);
	}

	@Override
	public void onRefresh() {
		getBooking(true, false);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		try {
			Serializer serializer = new Persister();
			if (contentID == 1013) {
				return serializer.read(StatusEntity.class, result);
			} else
				return serializer.read(ViewBookingEntity.class, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (contentID == 1013 && object instanceof StatusEntity) {
			StatusEntity mStatusEntity = (StatusEntity) object;
			boolean isError = mStatusEntity.getiStatus() != 0;
			if (isError) {
				Toast.makeText(getActivity(), mStatusEntity.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
						getActivity());
				dialogBuilder.setMessage(
						"Booking # " + bookingDeletedNumber
								+ " cancellation accepted.").setPositiveButton(
						"OK", new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								getBooking(true, true);
								dialog.dismiss();
							}
						});
				AlertDialog confirmDialog = dialogBuilder.create();
				confirmDialog.show();

			}
		} else if (object instanceof ViewBookingEntity) {
			ViewBookingEntity viewBooking = (ViewBookingEntity) object;
			boolean isError = viewBooking.getiStatus() != 0;
			if (isError) {
				Toast.makeText(getActivity(), viewBooking.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				if (swipeRefreshLayout.isRefreshing())
					swipeRefreshLayout.setRefreshing(false);
				if (!adapter.isEmpty())
					adapter.clean();

				if (viewBooking.getDatParam1() != null
						&& viewBooking.getDatParam1().getTeeTimeList() != null
						&& viewBooking.getDatParam1().getTeeTimeList().size() > 0)
					for (TeeTimeViewEntity en : viewBooking.getDatParam1()
							.getTeeTimeList()) {
						adapter.addData(en);
					}
				adapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onConnectionError(String errorMsg) {

	}

	@Override
	public void onCardClick(TeeTimeViewEntity mTeeTimeEntity) {
		confirmRemoveDialog(mTeeTimeEntity);
	}

	private void confirmRemoveDialog(final TeeTimeViewEntity mTeeTimeEntity) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage("Are you sure you want to delete this booking?");
		builder.setPositiveButton("Yes", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				bookingDeletedNumber = mTeeTimeEntity.getRef();
				Bundle deleteParam = new Bundle();
				deleteParam.putString("sLoginCD", qisoft.getmUserEntity()
						.getgsCD());
				deleteParam.putString("sClubCD", qisoft.getSelectedClub()
						.getClubcd());
				deleteParam.putString("lBookingID",
						String.valueOf(mTeeTimeEntity.getBookingid()));
				deleteParam.putString("sCheckSum", qisoft.getCheckSum());
				deleteParam.putString("sTokenCD", qisoft.getmUserEntity()
						.getToken());
				deleteParam.putString("sDeviceCD", qisoft.getDeviceId());
				deleteParam.putString("FragmentName",
						MyBookingFragment.fragmentName);
				deleteParam.putString("MethodName",
						QiSoftMethod.DELETE_BOOKING.toString());
				activity.getFragmentData(deleteParam, MyBookingFragment.this,
						1013);
			}
		});
		builder.setNegativeButton("No", null);
		builder.show();
	}

}
