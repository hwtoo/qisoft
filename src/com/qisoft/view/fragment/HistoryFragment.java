package com.qisoft.view.fragment;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.HistoryBookingRecyclerViewAdapter;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.view.QisoftApplication;
import com.qisoft.view.TeeTimeView;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.history.HistoryBookingEntity;
import com.qisoft.xmlparse.entity.viewbooking.ViewBookingEntity;

public class HistoryFragment extends Fragment implements onXmlParserCallBack {

	private QisoftApplication qisoft;
	private TeeTimeView activity;
	public static String fragmentName;

	private SwipeRefreshLayout swipeRefreshLayout = null;
	private RecyclerView mRecyclerView = null;
	private HistoryBookingRecyclerViewAdapter adapter;

	public static Fragment newInstance() {
		HistoryFragment status = new HistoryFragment();
		HistoryFragment.fragmentName = status.getClass().getName();
		return status;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof TeeTimeView) {
			activity = (TeeTimeView) getActivity();
			qisoft = activity.getQisoftApplication();
		}
		activity.getSupportActionBar().setTitle("View Booking");
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_viewbooking, container,
				false);

		swipeRefreshLayout = (SwipeRefreshLayout) v
				.findViewById(R.id.swipeLayout);
		swipeRefreshLayout.setEnabled(false);
		mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.setHasFixedSize(true);
		adapter = new HistoryBookingRecyclerViewAdapter();
		mRecyclerView.setAdapter(adapter);
		getHistory();
		return v;
	}

	private void getHistory() {
		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sClubCD", qisoft.getSelectedClub().getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putBoolean("LOADING", true);
		param.putString("FragmentName", HistoryFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.HISTORY_BOOKING.toString());
		activity.getFragmentData(param, this, 1014);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		try {
			Serializer serializer = new Persister();
			return serializer.read(HistoryBookingEntity.class, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (object instanceof HistoryBookingEntity) {
			HistoryBookingEntity historyBooking = (HistoryBookingEntity) object;
			boolean isError = historyBooking.getiStatus() != 0;
			if (isError) {
				Toast.makeText(getActivity(), historyBooking.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				if (!adapter.isEmpty())
					adapter.clean();
				int historyCount = historyBooking.getDatParam1()
						.getTeeTimeList().size() >= 20 ? 20 : historyBooking
						.getDatParam1().getTeeTimeList().size();
				for (int i = 0; i < historyCount; i++) {
					adapter.addData(historyBooking.getDatParam1()
							.getTeeTimeList().get(i));
				}
				adapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onConnectionError(String errorMsg) {

	}
}
