package com.qisoft.view.fragment;

import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.app.AlertDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.adapter.CourseDateAdapter;
import com.qisoft.adapter.TeeTimeGridViewAdapter;
import com.qisoft.util.GridViewScrollable;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.util.Util;
import com.qisoft.view.QisoftApplication;
import com.qisoft.view.TeeTimeView;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.clubCourseList.ClubCourseDateList;
import com.qisoft.xmlparse.entity.clubCourseList.ClubCourseList;
import com.qisoft.xmlparse.entity.teetime.TeeTimeEntity;
import com.qisoft.xmlparse.entity.teetime.TeeTimeList;
import com.qisoft.xmlparse.entity.teetimebooking.TeeTimeBookingEntity;

public class BookingFragment extends Fragment implements OnClickListener,
		OnTimeSetListener, onXmlParserCallBack, OnItemClickListener,
		OnItemSelectedListener {

	private QisoftApplication qisoft;
	private TeeTimeView activity;
	public static String fragmentName;

	private Spinner /* spinnerCourse = null, */spinneDate = null;
	private TextView textViewCourse = null, textViewLocation_1 = null,
			textViewLocation_2 = null, textViewLocation_3 = null;
	private EditText editTextPreferedFrom = null, editTextPreferedTo = null;
	private LinearLayout teeTimeLinear_1 = null, teeTimeLinear_2 = null,
			teeTimeLinear_3 = null;

	private GridViewScrollable mGridView_1 = null, mGridView_2 = null,
			mGridView_3;

	private Button bookingBtn = null;

	ClubCourseList mClubCourseList = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof TeeTimeView) {
			activity = (TeeTimeView) getActivity();
			qisoft = activity.getQisoftApplication();
		}
		activity.getSupportActionBar().setTitle("Golf Tee Time");
	}

	public static Fragment newInstance() {
		BookingFragment status = new BookingFragment();
		BookingFragment.fragmentName = status.getClass().getName();
		return status;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_booking, container, false);
		// spinnerCourse = (Spinner) v.findViewById(R.id.spinnerCourse);
		textViewCourse = (TextView) v.findViewById(R.id.textViewCourse);
		spinneDate = (Spinner) v.findViewById(R.id.spinnerDate);
		spinneDate.setOnItemSelectedListener(this);

		editTextPreferedFrom = (EditText) v
				.findViewById(R.id.editTextPreferedFrom);
		editTextPreferedFrom.setKeyListener(null);
		editTextPreferedFrom.setOnClickListener(this);
		editTextPreferedFrom.addTextChangedListener(textWacther);

		editTextPreferedTo = (EditText) v.findViewById(R.id.editTextPreferedTo);
		editTextPreferedTo.setKeyListener(null);
		editTextPreferedTo.setOnClickListener(this);
		editTextPreferedTo.addTextChangedListener(textWacther);

		teeTimeLinear_1 = (LinearLayout) v.findViewById(R.id.teetimeLocation1);
		textViewLocation_1 = (TextView) teeTimeLinear_1
				.findViewById(R.id.textViewLocation);
		mGridView_1 = (GridViewScrollable) teeTimeLinear_1
				.findViewById(R.id.gridViewId);
		mGridView_1.setExpanded(true);
		mGridView_1.setOnItemClickListener(this);

		teeTimeLinear_2 = (LinearLayout) v.findViewById(R.id.teetimeLocation2);
		textViewLocation_2 = (TextView) teeTimeLinear_2
				.findViewById(R.id.textViewLocation);
		mGridView_2 = (GridViewScrollable) teeTimeLinear_2
				.findViewById(R.id.gridViewId);
		mGridView_2.setExpanded(true);
		mGridView_2.setOnItemClickListener(this);

		teeTimeLinear_3 = (LinearLayout) v.findViewById(R.id.teetimeLocation3);
		textViewLocation_3 = (TextView) teeTimeLinear_3
				.findViewById(R.id.textViewLocation);
		mGridView_3 = (GridViewScrollable) teeTimeLinear_3
				.findViewById(R.id.gridViewId);
		mGridView_3.setExpanded(true);
		mGridView_3.setOnItemClickListener(this);

		bookingBtn = (Button) v.findViewById(R.id.bookingBtn);
		bookingBtn.setOnClickListener(this);

		getCourse();
		return v;
	}

	private TextWatcher textWacther = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			requestTeeTime();
		}
	};

	private void getCourse() {
		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("sName", qisoft.getmUserEntity().getName());
		param.putString("sClubCD", qisoft.getSelectedClub().getClubcd());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putString("FragmentName", BookingFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.GETCOURSE.toString());
		activity.getFragmentData(param, this, 1009);
	}

	private void getTeeTime() {
		ClubCourseDateList mClubCourseDateList = (ClubCourseDateList) spinneDate
				.getSelectedItem();
		if (mClubCourseList == null)
			return;

		Bundle param = new Bundle();
		param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
		param.putString("lClubCourseID", String.valueOf(mClubCourseList
				.getDatParam1().getClubCourseID()));
		param.putString("dteDate", mClubCourseDateList.getDateValue());
		param.putString("dteSTime", editTextPreferedFrom.getText().toString());
		param.putString("dteETime", editTextPreferedTo.getText().toString());
		param.putString("sCheckSum", qisoft.getCheckSum());
		param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
		param.putString("sDeviceCD", qisoft.getDeviceId());
		param.putString("FragmentName", BookingFragment.fragmentName);
		param.putString("MethodName", QiSoftMethod.GETTEETIME.toString());
		activity.getFragmentData(param, this, 1010);
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		String am_pm = (hourOfDay < 12) ? "AM" : "PM";
		String hour = String.format("%02d", hourOfDay);
		String minutes = String.format("%02d", minute);

		switch ((Integer) view.getTag()) {
		case R.id.editTextPreferedFrom:
			editTextPreferedFrom.setText(hour + ":" + minutes + " " + am_pm);
			break;

		case R.id.editTextPreferedTo:
			editTextPreferedTo.setText(hour + ":" + minutes + " " + am_pm);
			break;

		default:
			break;
		}
	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		try {
			Serializer serializer = new Persister();
			if (contentID == 1009) {
				return serializer.read(ClubCourseList.class, result);
			} else if (contentID == 1010) {
				return serializer.read(TeeTimeEntity.class, result);
			} else if (contentID == 1011) {
				return serializer.read(TeeTimeBookingEntity.class, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {
		if (contentID == 1009 && object instanceof ClubCourseList) {
			mClubCourseList = (ClubCourseList) object;
			boolean isError = mClubCourseList.getiStatus() != 0;
			if (isError) {
				Toast.makeText(getActivity(), mClubCourseList.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				spinneDate.setAdapter(new CourseDateAdapter(getActivity(),
						mClubCourseList.getDatParam2().getNewDataSet()));
				textViewCourse.setText(mClubCourseList.getDatParam1()
						.getClubCourseName());

			}
		} else if (contentID == 1010 && object instanceof TeeTimeEntity) {
			TeeTimeEntity mTeeTimeEntity = (TeeTimeEntity) object;
			boolean isError = mTeeTimeEntity.getiStatus() != 0;
			if (isError) {
				Toast.makeText(getActivity(), mTeeTimeEntity.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				if (mTeeTimeEntity.getsParam1() != null
						&& !mTeeTimeEntity.getsParam1().equals("")) {
					generateGridView(teeTimeLinear_1, textViewLocation_1,
							mTeeTimeEntity.getsParam1(), mGridView_1,
							mTeeTimeEntity.getDatParam1().getTeeTimeList());

					generateGridView(teeTimeLinear_2, textViewLocation_2,
							mTeeTimeEntity.getsParam2(), mGridView_2,
							mTeeTimeEntity.getDatParam2().getTeeTimeList());

					generateGridView(teeTimeLinear_3, textViewLocation_3,
							mTeeTimeEntity.getsParam3(), mGridView_3,
							mTeeTimeEntity.getDatParam3().getTeeTimeList());
					bookingBtn.setVisibility(View.VISIBLE);
				}
			}
		} else if (contentID == 1011 && object instanceof TeeTimeBookingEntity) {
			TeeTimeBookingEntity bookingResult = (TeeTimeBookingEntity) object;
			boolean isError = bookingResult.getiStatus() != 0;
			if (isError) {
				Toast.makeText(getActivity(), bookingResult.getsMsg(),
						Toast.LENGTH_SHORT).show();
			} else {
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
						getActivity());
				dialogBuilder.setMessage(
						"Your booking is confirmed. The reference number is "
								+ bookingResult.getDatParam1().getRefNumber())
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										cleanFragment();
										activity.showMyBooking();
									}
								});
				AlertDialog confirmDialog = dialogBuilder.create();
				confirmDialog.show();
			}
		}
	}

	private void cleanFragment() {
		spinneDate.setSelection(0);
		editTextPreferedFrom.setText("");
		editTextPreferedTo.setText("");
		closeGridView(teeTimeLinear_1, textViewLocation_1, mGridView_1);
		closeGridView(teeTimeLinear_2, textViewLocation_2, mGridView_2);
		closeGridView(teeTimeLinear_3, textViewLocation_3, mGridView_3);
		bookingBtn.setVisibility(View.GONE);

	}

	private void closeGridView(LinearLayout teeTimeLayout,
			TextView textViewTitle, GridViewScrollable gridView) {
		teeTimeLayout.setVisibility(View.GONE);
		textViewTitle.setText("");
		if (!gridView.getAdapter().isEmpty())
			gridView.setAdapter(null);
	}

	private void generateGridView(LinearLayout teeTimeLayout,
			TextView textViewTitle, String title, GridViewScrollable gridView,
			List<TeeTimeList> dataAry) {
		teeTimeLayout.setVisibility(View.VISIBLE);
		textViewTitle.setText(title);
		TeeTimeGridViewAdapter adapter = new TeeTimeGridViewAdapter(
				getActivity(), dataAry);
		gridView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onConnectionError(String errorMsg) {

	}

	View V = null;
	TeeTimeList selectedTime = null;

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// if (V != null)
		// V.setActivated(false);
		// V = view;
		selectedTime = (TeeTimeList) parent.getItemAtPosition(position);

		int parentNumber;
		if (parent == mGridView_1)
			parentNumber = 0;
		else if (parent == mGridView_2)
			parentNumber = 1;
		else
			parentNumber = 2;

		changeChildSelected(parentNumber, position);
	}

	private void changeChildSelected(int parentNumber, int position) {
		switch (parentNumber) {
		case 0:
			mGridView_2.setSelection(-1);
			mGridView_2.clearChoices();
			mGridView_3.setSelection(-1);
			mGridView_3.clearChoices();
			break;
		case 1:
			mGridView_1.setSelection(-1);
			mGridView_1.clearChoices();
			mGridView_3.setSelection(-1);
			mGridView_3.clearChoices();
			break;

		case 2:
			mGridView_1.setSelection(-1);
			mGridView_1.clearChoices();
			mGridView_2.setSelection(-1);
			mGridView_2.clearChoices();
			break;

		default:
			break;
		}
	}

	private void requestTeeTime() {
		if (!editTextPreferedFrom.getText().toString().equals("")
				&& !editTextPreferedTo.getText().toString().equals("")) {
			getTeeTime();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		requestTeeTime();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bookingBtn:
			Bundle param = new Bundle();
			param.putString("sLoginCD", qisoft.getmUserEntity().getgsCD());
			param.putString("lClubCourseID", String.valueOf(mClubCourseList
					.getDatParam1().getClubCourseID()));
			param.putInt("lTeeTimeID", selectedTime.getTeeTimeId());
			param.putString("sCheckSum", qisoft.getCheckSum());
			param.putString("sTokenCD", qisoft.getmUserEntity().getToken());
			param.putString("sDeviceCD", qisoft.getDeviceId());
			param.putString("FragmentName", BookingFragment.fragmentName);
			param.putString("MethodName", QiSoftMethod.BOOKING.toString());
			activity.getFragmentData(param, this, 1011);
			break;

		case R.id.editTextPreferedFrom:
			Util.getTimeFromTimePicker(v.getId(), getActivity(), this, 7);
			break;
		case R.id.editTextPreferedTo:
			Util.getTimeFromTimePicker(v.getId(), getActivity(), this, 8);
			break;
		default:
			break;
		}

	}

}