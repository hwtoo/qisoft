package com.qisoft.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.qisoft.R;
import com.qisoft.adapter.MembershipPageAdapter;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.view.fragment.ProfileFragment;
import com.qisoft.view.fragment.StatusFragment;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.accountStatus.AccountStatusEntity;
import com.qisoft.xmlparse.entity.clubCourseList.ClubCourseList;
import com.qisoft.xmlparse.entity.history.HistoryBookingEntity;
import com.qisoft.xmlparse.entity.profileGet.ProfileGetEntity;
import com.qisoft.xmlparse.entity.viewbooking.ViewBookingEntity;

public class MembershipView extends AbstractSlidingActivity {

	private AccountStatusEntity accountStatus = null;
	private ProfileGetEntity profileEntity = null;

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentId) {
		String fragmentName = (String) localParam.getString("FragmentName");
		for (onXmlParserCallBack fragmentListener : fragementListner) {
			if (fragmentListener.getClass().getName()
					.equalsIgnoreCase(fragmentName))
				return fragmentListener.xmlSerializerData(result, localParam,
						contentId);
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentId) {
		String fragmentName = (String) localParam.getString("FragmentName");
		for (onXmlParserCallBack fragmentListener : fragementListner) {
			if (fragmentListener.getClass().getName()
					.equalsIgnoreCase(fragmentName)) {
				switch (QiSoftMethod
						.valueOf(localParam.getString("MethodName"))) {
				case GET_STATUS:
					accountStatus = (AccountStatusEntity) object;
					break;

				case GET_PROFILE:
					profileEntity = (ProfileGetEntity) object;
					break;

				default:
					break;
				}
				fragmentListener.onSerializerComplete(object, localParam,
						contentId);
			}
		}
	}

	public void getFragmentData(Bundle bundle, onXmlParserCallBack listener,
			int ContentId) {
		if (!fragementListner.contains(listener))
			fragementListner.add(listener);
		String fragmentClass = bundle.getString("FragmentName");
		String methodName = bundle.getString("MethodName");

		if (fragmentClass.equalsIgnoreCase(StatusFragment.fragmentName)
				&& accountStatus != null) {
			listener.onSerializerComplete(accountStatus, bundle, ContentId);
		} 
		// else if (fragmentClass.equalsIgnoreCase(ProfileFragment.fragmentName)
		// && profileEntity != null
		// && methodName.equals(QiSoftMethod.GET_PROFILE.toString())) {
		// listener.onSerializerComplete(profileEntity, bundle, ContentId);
		// } 
		else {
			getData(ContentId, bundle);
		}
	}

	@Override
	public void onBackPressed() {
		Fragment page = getSupportFragmentManager().findFragmentByTag(
				"android:switcher:" + R.id.viewPagerID + ":"
						+ viewPager.getCurrentItem());
		if (page.getClass().getName()
				.equalsIgnoreCase(ProfileFragment.fragmentName)
				&& ((ProfileFragment) page).getEditableFlag()) {
			((ProfileFragment) page).changeFlag(false, true);
		} else
			super.onBackPressed();
	}

	@Override
	public FragmentPagerAdapter getAdapter() {
		return new MembershipPageAdapter(getSupportFragmentManager());
	}
}
