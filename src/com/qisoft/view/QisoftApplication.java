package com.qisoft.view;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings.Secure;

import com.qisoft.xmlparse.entity.UserClub;
import com.qisoft.xmlparse.entity.UserEntity;

public class QisoftApplication extends Application {

	private String checkSum = "";
	private String deviceId = "";
	private SharedPreferences prefs = null;
	private UserEntity mUserEntity = null;
	private UserClub selectedClub = null;

	public QisoftApplication() {

	}

	@Override
	public void onCreate() {
		super.onCreate();
		deviceId = getPreference().getString("ANDROID_ID", null);
		if (deviceId == null) {
			deviceId = Secure
					.getString(getContentResolver(), Secure.ANDROID_ID);
			getPreference().edit().putString("ANDROID_ID", deviceId).commit();
		}
		generateCheckSum();
	}

	public SharedPreferences getPreference() {
		if (prefs == null)
			prefs = this.getSharedPreferences("QISOFT", MODE_PRIVATE);
		return prefs;
	}

	private void generateCheckSum() {
		this.checkSum = "testing123";
	}

	public void storeEmailAndPassword(String email, String password) {
		if (prefs == null)
			getPreference();
		Editor editor = prefs.edit();
		editor.putString("email", email);
		editor.putString("password", password);
		editor.commit();
	}

	public String getUserEmail() {
		if (prefs == null)
			getPreference();
		return prefs.getString("email", null);
	}

	public String getUserPassword() {
		if (prefs == null)
			getPreference();
		return prefs.getString("password", null);
	}

	public void logoutUser() {
		if (prefs == null)
			getPreference();
		prefs.edit().remove("email").commit();
		prefs.edit().remove("password").commit();
	}

	public String getDeviceId() {
		return this.deviceId;
	}

	public String getCheckSum() {
		return checkSum;
	}

	public void setmUserEntity(UserEntity mUserEntity) {
		this.mUserEntity = mUserEntity;
	}

	public UserEntity getmUserEntity() {
		return mUserEntity;
	}

	public void setSelectedClub(UserClub selectedClub) {
		this.selectedClub = selectedClub;
	}

	public UserClub getSelectedClub() {
		return selectedClub;
	}
}
