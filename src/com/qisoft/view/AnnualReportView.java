package com.qisoft.view;

import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.UserClubMenu;

public class AnnualReportView extends AbstractActivity implements
		OnItemClickListener {

	private ListView mListview = null;
	private AnnualReportAdatper adapter = null;
	private ArrayList<UserClubMenu> data = new ArrayList<UserClubMenu>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_clublist);
		getSupportActionBar().setTitle("Annual Report");

		data = (ArrayList<UserClubMenu>) qisoft.getmUserEntity().getGdatMenu()
				.getNewDataSet();
		
		mListview = (ListView) findViewById(R.id.clubListView);
		adapter = new AnnualReportAdatper();
		mListview.setAdapter(adapter);
		mListview.setOnItemClickListener(this);

		// for (UserClubMenu userClub :
		// qisoft.getmUserEntity().getGdatMenu().getNewDataSet())
		// adapter.addData(userClub);
		// adapter.notifyDataSetChanged();

	}

	@Override
	public Object xmlSerializerData(String result, Bundle param, int contentID) {
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle param, int contentID) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri
				.parse(data.get(position).getHyperlink()));
		startActivity(intent);
	}

	private class AnnualReportAdatper extends BaseAdapter {

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = getLayoutInflater().inflate(
						R.layout.child_clublist, parent, false);
				holder.year = (TextView) convertView
						.findViewById(R.id.clubName);
				convertView.setTag(holder);
			} else
				holder = (ViewHolder) convertView.getTag();

			if (holder.year != null)
				holder.year.setText(data.get(position).getMenuname());
			return convertView;
		}
	}

	class ViewHolder {
		TextView year;
	}

}
