package com.qisoft.view;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;

import com.qisoft.adapter.TeeTimeViewPager;
import com.qisoft.util.QiSoftMethod;
import com.qisoft.view.fragment.BookingFragment;
import com.qisoft.view.fragment.HistoryFragment;
import com.qisoft.view.fragment.MyBookingFragment;
import com.qisoft.xmlparse.onXmlParserCallBack;
import com.qisoft.xmlparse.entity.clubCourseList.ClubCourseList;
import com.qisoft.xmlparse.entity.history.HistoryBookingEntity;
import com.qisoft.xmlparse.entity.viewbooking.ViewBookingEntity;

public class TeeTimeView extends AbstractSlidingActivity {
	private ClubCourseList mClubCourseList = null;
	private ViewBookingEntity viewBooking = null;
	private HistoryBookingEntity historyBooking = null;

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentID) {
		String fragmentName = (String) localParam.getString("FragmentName");
		for (onXmlParserCallBack fragmentListener : fragementListner) {
			if (fragmentListener.getClass().getName()
					.equalsIgnoreCase(fragmentName))
				return fragmentListener.xmlSerializerData(result, localParam,
						contentID);
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentID) {
		String fragmentName = (String) localParam.getString("FragmentName");
		for (onXmlParserCallBack fragmentListener : fragementListner) {
			if (fragmentListener.getClass().getName()
					.equalsIgnoreCase(fragmentName)) {
				switch (QiSoftMethod.valueOf(localParam
						.getString("MethodName"))) {
				case GETCOURSE:
					mClubCourseList = (ClubCourseList) object;
					break;
				case VIEW_BOOKING:
					viewBooking = (ViewBookingEntity) object;
					break;

				case HISTORY_BOOKING:
					historyBooking = (HistoryBookingEntity) object;
					break;

				default:
					break;
				}
				fragmentListener.onSerializerComplete(object, localParam,
						contentID);
			}
		}
	}

	public void getFragmentData(Bundle bundle, onXmlParserCallBack listener,
			int ContentId) {
		if (!fragementListner.contains(listener))
			fragementListner.add(listener);
		String fragmentClass = bundle.getString("FragmentName");
		String methodName = bundle.getString("MethodName");

		if (fragmentClass.equalsIgnoreCase(BookingFragment.fragmentName)
				&& methodName.equals(QiSoftMethod.GETCOURSE.toString())
				&& mClubCourseList != null) {
			listener.onSerializerComplete(mClubCourseList, bundle, ContentId);
		} else if (fragmentClass
				.equalsIgnoreCase(MyBookingFragment.fragmentName)
				&& methodName.equals(QiSoftMethod.VIEW_BOOKING.toString())
				&& viewBooking != null && !bundle.getBoolean("RefreshData")) {
			listener.onSerializerComplete(viewBooking, bundle, ContentId);
		} else if (fragmentClass.equalsIgnoreCase(HistoryFragment.fragmentName)
				&& methodName.equals(QiSoftMethod.HISTORY_BOOKING.toString())
				&& historyBooking != null) {
			listener.onSerializerComplete(historyBooking, bundle, ContentId);
		} else {
			getData(ContentId, bundle);
		}
	}

	@Override
	public FragmentPagerAdapter getAdapter() {
		return new TeeTimeViewPager(getSupportFragmentManager());
	}

	private boolean refreshViewBookingList = false;

	public boolean getRefreshViewBookingList() {
		return refreshViewBookingList;
	}

	public void resetRefreshViewBookingList() {
		if (refreshViewBookingList)
			refreshViewBookingList = false;
	}

	public void showMyBooking() {
		refreshViewBookingList = true;
		viewPager.setCurrentItem(0, true);
	}

}
