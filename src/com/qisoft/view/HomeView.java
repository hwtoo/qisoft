package com.qisoft.view;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;

import com.qisoft.adapter.HomePageViewPager;
import com.qisoft.xmlparse.onXmlParserCallBack;

public class HomeView extends AbstractSlidingActivity {

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentId) {
		String fragmentName = (String) localParam.getString("FragementName");
		for (onXmlParserCallBack fragmentListener : fragementListner) {
			if (fragmentListener.getClass().getName()
					.equalsIgnoreCase(fragmentName))
				return fragmentListener.xmlSerializerData(result, localParam,
						contentId);
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentId) {
		String fragmentName = (String) localParam.getString("FragementName");
		for (onXmlParserCallBack fragmentListener : fragementListner) {
			if (fragmentListener.getClass().getName()
					.equalsIgnoreCase(fragmentName))
				fragmentListener.onSerializerComplete(object, localParam,
						contentId);
		}
	}

	@Override
	public FragmentPagerAdapter getAdapter() {
		return new HomePageViewPager(getSupportFragmentManager());
	}

}
