package com.qisoft.view;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.qisoft.R;
import com.qisoft.xmlparse.entity.StatusEntity;

public class ForgetPassword extends AbstractActivity implements OnClickListener {

	private EditText loginIdEditText = null, loginEmailEditText = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_passsword);
		loginIdEditText = (EditText) findViewById(R.id.loginIDEditText);
		loginEmailEditText = (EditText) findViewById(R.id.loginEmailEditText);
		findViewById(R.id.LoginIDBtn).setOnClickListener(this);
		findViewById(R.id.LoginEmailBtn).setOnClickListener(this);
	}

	@Override
	public Object xmlSerializerData(String result, Bundle localParam,
			int contentId) {
		System.out.println("%%%%% xmlSerializerData");
		Serializer serializer = new Persister();
		try {
			return serializer.read(StatusEntity.class, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onSerializerComplete(Object object, Bundle localParam,
			int contentId) {
		if (object instanceof StatusEntity) {
			StatusEntity userEntity = (StatusEntity) object;
			int status = userEntity.getiStatus();
			if (status == 0) {
				Toast.makeText(this,
						"Your password sent to your email address.",
						Toast.LENGTH_SHORT).show();
				finish();
			} else {
				Toast.makeText(this, userEntity.getsMsg(), Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.LoginIDBtn:
			String loignIdText = loginIdEditText.getText().toString();
			if (!loignIdText.isEmpty()) {
				Bundle param = new Bundle();
				param.putString("login_id", loignIdText);
				param.putString("sCheckSum", qisoft.getCheckSum());
				getData(1003, this, param);
			}

			break;

		case R.id.LoginEmailBtn:
			String loginEmailText = loginEmailEditText.getText().toString();
			if (!loginEmailText.isEmpty()) {
				Bundle param = new Bundle();
				param.putString("email_id", loginEmailText);
				param.putString("sCheckSum", qisoft.getCheckSum());
				getData(1003, this, param);
			}
			break;
		default:
			break;
		}
	}
}
